CREATE DATABASE IF NOT EXISTS polyclinics;

USE polyclinics;

CREATE TABLE IF NOT EXISTS u_user (
  personal_identifier BIGINT(13) UNSIGNED PRIMARY KEY NOT NULL,
  first_name          VARCHAR(50) NOT NULL,
  last_name           VARCHAR(50) NOT NULL,
  address             VARCHAR(100) NOT NULL,
  phone_number        VARCHAR(12) NOT NULL,
  email               VARCHAR(60) NOT NULL,
  IBAN_account		VARCHAR(21) NOT NULL,
  contract_number		VARCHAR(11) NOT NULL,
  hire_date			VARCHAR(11) NOT NULL,
  position 			VARCHAR(40) NOT NULL,
  username            VARCHAR(30) NOT NULL ,
  password            VARCHAR(30) NOT NULL
);

ALTER TABLE u_user ADD CONSTRAINT u_user_ck_position_possible_values CHECK (position IN
                                                                            ('administrator','super-administrator','inspector resurse umane','expert financiar-contabil','receptioner','asistent medical','medic'));

CREATE TABLE IF NOT EXISTS u_employee (
  personal_identifier BIGINT(13) UNSIGNED PRIMARY KEY NOT NULL,
  salary 				FLOAT(8,2) UNSIGNED NOT NULL,
  working_hours		INT(4)	UNSIGNED NOT NULL,
  FOREIGN KEY (personal_identifier) REFERENCES u_user(personal_identifier) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS u_schedule(
  id 					INT(6) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  time_slot			VARCHAR(14) NOT NULL,
  weekday				VARCHAR(9),
  schedule_date		VARCHAR(12),
  location			VARCHAR(30),
  id_employee			BIGINT(13) UNSIGNED NOT NULL,
  FOREIGN KEY (id_employee) REFERENCES u_employee(personal_identifier) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS u_assistant(
  personal_identifier BIGINT(13) UNSIGNED PRIMARY KEY NOT NULL,
  speciality			VARCHAR(11) NOT NULL,
  degree				VARCHAR(10) NOT NULL,
  FOREIGN KEY (personal_identifier) REFERENCES u_employee(personal_identifier) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE u_assistant ADD CONSTRAINT u_assistant_ck_speciality_possible_values CHECK (speciality IN ('generalist','laborator','radiologie'));
ALTER TABLE u_assistant ADD CONSTRAINT u_assistant_ck_degree_possible_values CHECK (degree IN ('secundar','principal'));


CREATE TABLE IF NOT EXISTS u_doctor (
  personal_identifier BIGINT(13) UNSIGNED PRIMARY KEY NOT NULL,
  paraph_code 		VARCHAR(8) NOT NULL UNIQUE,
  scientific_title	VARCHAR(50),
  didactic_chair		VARCHAR(40),
  FOREIGN KEY (personal_identifier) REFERENCES u_employee(personal_identifier) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE u_doctor ADD CONSTRAINT u_doctor_ck_scientific_title_possible_values CHECK (scientific_title IN ('-','doctorand','doctor in stiinte medicale'));
ALTER TABLE u_doctor ADD CONSTRAINT u_doctor_ck_didactic_chair_possible_values CHECK (didactic_chair IN ('preparator','asistent','sef de lucrari','conferentiar','profesor'));

CREATE TABLE IF NOT EXISTS u_speciality_doctor(
  id 					INT(6)	UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  speciality			VARCHAR(60) NOT NULL,
  degree				VARCHAR(60) NOT NULL,
  id_doctor BIGINT(13) UNSIGNED NOT NULL,
  FOREIGN KEY (id_doctor) REFERENCES u_doctor(personal_identifier) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE u_speciality_doctor ADD CONSTRAINT u_speciality_doctor_ck_degree_possible_values CHECK (degree IN ('specialist','primar'));

CREATE TABLE IF NOT EXISTS u_ability(
  id 						INT(6) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  name				 	VARCHAR(50) NOT NULL,
  id_speciality_doctor	INT(6) UNSIGNED NOT NULL,
  FOREIGN KEY (id_speciality_doctor) REFERENCES u_speciality_doctor(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS speciality(
  id 					INT(6) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  name				VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS investigation(
  id 					INT(6) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
  name 				VARCHAR(250) NOT NULL,
  duration			INT(3) NOT NULL,
  price				DOUBLE(8,2) NOT NULL,
  ability				VARCHAR(40) DEFAULT '-',
  id_speciality		INT(6) UNSIGNED NOT NULL,
  FOREIGN KEY (id_speciality) REFERENCES speciality(id) ON UPDATE CASCADE ON DELETE CASCADE
);