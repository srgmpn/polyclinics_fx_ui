package com.srg.dao.impl;

import com.srg.dao.BasicCRUDDao;
import com.srg.model.Employee;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class EmployeeDaoImplTest {

    BasicCRUDDao<Employee> employeeDao;
    Employee emp1;
    Employee emp2;
    Employee empAss2;
    Employee empDc1;

    @Before
    public void setUp() throws Exception {
        employeeDao = new EmployeeDaoImpl("employee");
        emp1 = new Employee();
        emp1.setPersonal_identifier("1200211304601");
        emp1.setSalary("2500");
        emp1.setWorking_hours("35");

        emp2 = new Employee();
        emp2.setPersonal_identifier("1730320130184");
        emp2.setSalary("2600");
        emp2.setWorking_hours("25");

        empAss2 = new Employee();
        empAss2.setPersonal_identifier("1770902909244");
        empAss2.setSalary("7974");
        empAss2.setWorking_hours("111");

        empDc1 = new Employee();
        empDc1.setPersonal_identifier("1171129225779");
        empDc1.setSalary("4801");
        empDc1.setWorking_hours("116");
    }

    @Test
    public void testAdd() throws Exception {
        employeeDao.add(emp1);
        employeeDao.add(emp2);
        employeeDao.add(empAss2);
        employeeDao.add(empDc1);
    }

    @Test
    public void testDelete() throws Exception {
        employeeDao.delete("1200211304601");
        employeeDao.delete("1730320130184");
        employeeDao.delete(empAss2.getPersonal_identifier());
    }

    @Test
    public void testGetById() throws Exception {
        show(employeeDao.getById("1200211304601"));
    }

    @Test
    public void testGetAll() throws Exception {
        List<Employee> employeeList = employeeDao.getAll();

        for (Employee entity : employeeList){
            show(entity);
        }
    }

    @Test
    public void testUpdate() throws Exception {
        emp1.setSalary("40000");
        employeeDao.update(emp1);
    }

    private void show(Employee entity){
        System.out.println("--------------------------------------");
        System.out.println(entity.getPersonal_identifier());
        System.out.println(entity.getSalary());
        System.out.println(entity.getWorking_hours());

    }
}