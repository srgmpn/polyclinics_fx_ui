package com.srg.dao.impl;

import com.srg.dao.BasicCRUDDao;
import com.srg.model.SpecialityDoctor;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SpecialityDoctorDaoImplTest {

    BasicCRUDDao<SpecialityDoctor> specialityDoctorDao;
    SpecialityDoctor sp1, sp2;

    @Before
    public void setUp() throws Exception {
        specialityDoctorDao = new SpecialityDoctorDaoImpl("speciality_doctor");
        sp1 = new SpecialityDoctor();
        sp1.setSpeciality("psihiatrie");
        sp1.setDegree("primar");
        sp1.setId_doctor("1171129225779");
/*
        sp2 = new SpecialityDoctor();
        sp2.setSpeciality("");
        sp2.setDegree("");
        sp2.setId_doctor();*/
    }

    @Test
    public void testAdd() throws Exception {
        specialityDoctorDao.add(sp1);
    }

    @Test
    public void testDelete() throws Exception {
        specialityDoctorDao.delete("1");
    }

    @Test
    public void testGetById() throws Exception {
        SpecialityDoctor specialityDoctor = specialityDoctorDao.getById("1");
        show(specialityDoctor);
    }

    @Test
    public void testGetAll() throws Exception {
        List<SpecialityDoctor> specialityDoctors = specialityDoctorDao.getAll();

        for (SpecialityDoctor specialityDoctor : specialityDoctors){
            show(specialityDoctor);
        }
    }

    @Test
    public void testUpdate() throws Exception {

    }

    private void show(SpecialityDoctor entity){
        System.out.println(entity.getId());
        System.out.println(entity.getSpeciality());
        System.out.println(entity.getDegree());
        System.out.println(entity.getId_doctor());

    }
}