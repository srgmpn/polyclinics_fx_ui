package com.srg.dao.impl;

import com.srg.dao.BasicCRUDDao;
import com.srg.model.Schedule;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ScheduleDaoImplTest {

    BasicCRUDDao<Schedule> scheduleDao;
    Schedule sch1, sch2, sch3;


    @Before
    public void setUp() throws Exception {
        scheduleDao = new ScheduleDaoImpl("schedule");

        sch1 = new Schedule();
        sch1.setId("1");
        sch1.setWeekday("marti");
        sch1.setTime_slot("10:45-15:40");
        sch1.setLocation("Iasi");
        sch1.setId_employee("1770902909244");

        sch2 = new Schedule();
        //sch2.setId(2);
        sch2.setWeekday("miercuri");
        sch2.setTime_slot("17:25-18:20");
        sch2.setLocation("Cluj-Napoca");
        sch2.setId_employee("1770902909244");

        sch3 = new Schedule();
        sch3.setWeekday("sambata");
        sch3.setTime_slot("09:45-17:40");
        sch3.setLocation("Bucuresti");
        sch3.setId_employee("1770902909244");
    }

    @Test
    public void testAdd() throws Exception {
        scheduleDao.add(sch1);
        scheduleDao.add(sch2);
        scheduleDao.add(sch3);
    }

    @Test
    public void testDelete() throws Exception {
        scheduleDao.delete(sch1.getId());
        scheduleDao.delete(sch2.getId());
        scheduleDao.delete(sch3.getId());
    }

    @Test
    public void testGetById() throws Exception {
        Schedule schedule = scheduleDao.getById("2");
        show(schedule);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Schedule> schedules = scheduleDao.getAll();

        for (Schedule schedule : schedules){
            show(schedule);
        }
    }

    @Test
    public void testUpdate() throws Exception {
        sch1.setWeekday("luni");
        scheduleDao.update(sch1);
    }

    private void show(Schedule entity){
        System.out.println("--------------------------------------");
        System.out.println(entity.getId());
        System.out.println(entity.getWeekday());
        System.out.println(entity.getSchedule_date());
        System.out.println(entity.getLocation());
        System.out.println(entity.getId_employee());

    }

}