package com.srg.dao.impl;

import com.srg.dao.BasicCRUDDao;
import com.srg.model.Investigation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class InvestigationDaoImplTest {

    BasicCRUDDao<Investigation> basicDao;
    Investigation inv1, inv2;

    @Before
    public void setUp() throws Exception {
        basicDao = new InvestigationDaoImpl("investigation");

        inv1 = new Investigation();
//        inv1.setId(1);
        inv1.setName("Bicarbonat");
        inv1.setDuration("4");
        inv1.setPrice("60.0");
        inv1.setAbility("asistent medical");
        inv1.setId_speciality("1");
    }

    @Test
    public void testAdd() throws Exception {
        basicDao.add(inv1);
    }

    @Test
    public void testDelete() throws Exception {
        basicDao.delete(inv1.getId());
    }

    @Test
    public void testGetById() throws Exception {
        Investigation investigation = basicDao.getById("1");
        show(investigation);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Investigation> investigations = basicDao.getAll();

        for (Investigation investigation : investigations){
            show(investigation);
        }
    }

    @Test
    public void testUpdate() throws Exception {

    }

    private void show(Investigation entity){
        System.out.println("--------------------------------------");
        System.out.println(entity.getId());
        System.out.println(entity.getName());
        System.out.println(entity.getAbility());
        System.out.println(entity.getDuration());
        System.out.println(entity.getPrice());
        System.out.println(entity.getId_speciality());

    }
}