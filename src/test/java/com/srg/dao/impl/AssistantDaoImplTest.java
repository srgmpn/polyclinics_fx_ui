package com.srg.dao.impl;

import com.srg.dao.BasicCRUDDao;
import com.srg.model.Assistant;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class AssistantDaoImplTest {

    BasicCRUDDao<Assistant> assistantDao;
    Assistant ass1;
    Assistant ass2;

    @Before
    public void setUp() throws Exception {
        assistantDao = new AssistantDaoImpl("assistant");
        ass1 = new Assistant();
        ass1.setPersonal_identifier("1010521178499");
        ass1.setSpeciality("laborator");
        ass1.setDegree("principal");

        ass2 = new Assistant();
        ass2.setPersonal_identifier("1770902909244");
        ass2.setSpeciality("generalist");
        ass2.setDegree("secundar");
    }

    @Test
    public void testAdd() throws Exception {
       // assistantDao.add(ass1);
        assistantDao.add(ass2);
    }

    @Test
    public void testDelete() throws Exception {
        assistantDao.delete(ass2.getPersonal_identifier());
    }

    @Test
    public void testGetById() throws Exception {
        Assistant assistant = assistantDao.getById(ass2.getPersonal_identifier());
        show(assistant);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Assistant> assistants = assistantDao.getAll();

        for (Assistant assistant : assistants){
            show(assistant);
        }
    }

    @Test
    public void testUpdate() throws Exception {

    }

    private void show(Assistant entity){
        System.out.println("--------------------------------------");
        System.out.println(entity.getPersonal_identifier());
        System.out.println(entity.getSpeciality());
        System.out.println(entity.getDegree());

    }
}