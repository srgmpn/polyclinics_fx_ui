package com.srg.dao.impl;

import com.srg.dao.BasicCRUDDao;
import com.srg.model.Speciality;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SpecialityDaoImplTest {

    BasicCRUDDao<Speciality> basicDao;
    Speciality sp1, sp2;

    @Before
    public void setUp() throws Exception {

        basicDao = new SpecialityDaoImpl("speciality");
        sp1 = new Speciality();
        sp1.setId("1");
        sp1.setName("analize de laborator");

    }

    @Test
    public void testAdd() throws Exception {
        basicDao.add(sp1);
    }

    @Test
    public void testDelete() throws Exception {

        basicDao.delete(sp1.getId());
    }

    @Test
    public void testGetById() throws Exception {
        Speciality sp = basicDao.getById(sp1.getId());
        show(sp);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Speciality> specialities = basicDao.getAll();

        for (Speciality speciality : specialities){
            show(speciality);
        }
    }

    @Test
    public void testUpdate() throws Exception {

    }

    private void show(Speciality entity){
        System.out.println("--------------------------------------");
        System.out.println(entity.getId());
        System.out.println(entity.getName());

    }
}