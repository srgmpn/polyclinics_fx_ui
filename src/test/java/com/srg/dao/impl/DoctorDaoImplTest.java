package com.srg.dao.impl;

import com.srg.dao.BasicCRUDDao;
import com.srg.model.Doctor;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class DoctorDaoImplTest {

    BasicCRUDDao<Doctor> doctorDao;
    Doctor dc1, dc2;

    @Before
    public void setUp() throws Exception {
        doctorDao = new DoctorDaoImpl("doctor");

        dc1 = new Doctor();
        dc1.setPersonal_identifier("1171129225779");
        dc1.setParaph_code("329148");
        dc1.setScientific_title("doctor in stiinte medicale");
        dc1.setDidactic_chair("asistent universitar");

    }

    @Test
    public void testAdd() throws Exception {
       doctorDao.add(dc1);
    }

    @Test
    public void testDelete() throws Exception {
        doctorDao.delete(dc1.getPersonal_identifier());
    }

    @Test
    public void testGetById() throws Exception {
        Doctor doctor = doctorDao.getById(dc1.getPersonal_identifier());
        show(doctor);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Doctor> doctors = doctorDao.getAll();

        for (Doctor doctor : doctors){
            show(doctor);
        }
    }

    @Test
    public void testUpdate() throws Exception {
        dc1.setParaph_code("100000");
        doctorDao.update(dc1);
    }

    private void show(Doctor entity){
        System.out.println("--------------------------------------");
        System.out.println(entity.getPersonal_identifier());
        System.out.println(entity.getParaph_code());
        System.out.println(entity.getScientific_title());
        System.out.println(entity.getDidactic_chair());

    }
}