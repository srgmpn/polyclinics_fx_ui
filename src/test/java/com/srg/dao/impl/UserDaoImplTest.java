package com.srg.dao.impl;

import com.srg.dao.BasicCRUDDao;
import com.srg.model.User;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class UserDaoImplTest {
    BasicCRUDDao<User> userDao;
    User user1;
    User user2;
    User user3;
    User user4;
    User userEmpAss2;
    User userDc1;

    @Before
    public void setUp(){
        userDao = new UserDaoImpl("user");
        user1 = new User();
        user1.setPersonal_identifier("1200211304601");
        user1.setFirst_name("Isabela");
        user1.setLast_name("CONSTANTIN");
        user1.setAddress("Calarasi, Calarasi");
        user1.setPhone_number("0705810749");
        user1.setEmail("isabela.constantin@aim.com");
        user1.setIBAN_account("FR76BNRB1544357918");
        user1.setContract_number("IZWS760257");
        user1.setHire_date("24/6/1990");
        user1.setPosition("super-administrator");
        user1.setUsername("isabela.constantin");
        user1.setPassword("N.IuGCAwZY");

        user2 = new User();
        user2.setPersonal_identifier("1080304940711");
        user2.setFirst_name("Luiza");
        user2.setLast_name("VOINEA");
        user2.setAddress("Pitesti,VOINEA");
        user2.setPhone_number("0706890639");
        user2.setEmail("luiza.voinea@fastmail.com");
        user2.setIBAN_account("IE4RZBR626897265776");
        user2.setContract_number("YEKA544035");
        user2.setHire_date("16/11/2006");
        user2.setPosition("administrator");
        user2.setUsername("luiza.voinea");
        user2.setPassword("giw2G:-t/|");

        user3 = new User();
        user3.setPersonal_identifier("2121215788529");
        user3.setFirst_name("Mircea");
        user3.setLast_name("DIACONU");
        user3.setAddress("Bacau,Bacau");
        user3.setPhone_number("0703910362");
        user3.setEmail("mircea.diaconu@hotmail.com");
        user3.setIBAN_account("PT43CARP39717896559");
        user3.setContract_number("HLHA423869");
        user3.setHire_date("13/7/2006");
        user3.setPosition("expert financiar-contabil");
        user3.setUsername("mircea.diaconu");
        user3.setPassword("Z~f9R2XK7k");

        user4 = new User();
        user4.setPersonal_identifier("1730320130184");
        user4.setFirst_name("Georgeta");
        user4.setLast_name("MOCANU");
        user4.setAddress("Baia-Mare, Maramures");
        user4.setPhone_number("0702910341");
        user4.setEmail("georgeta.mocanu@in.com");
        user4.setIBAN_account("CY73BUCU163109792824");
        user4.setContract_number("TAZG601839");
        user4.setHire_date("5/9/1996");
        user4.setPosition("expert financiar-contabil");
        user4.setUsername("georgeta.mocanu");
        user4.setPassword("IJ*@}01;u4");

        userEmpAss2 = new User();
        userEmpAss2.setPersonal_identifier("1770902909244");
        userEmpAss2.setFirst_name("Cosmin");
        userEmpAss2.setLast_name("GHEORGHE");
        userEmpAss2.setAddress("Slatina, Olt");
        userEmpAss2.setPhone_number("0707800951");
        userEmpAss2.setEmail("cosmin.gheorghe@bigstring.com");
        userEmpAss2.setIBAN_account("DK53NBOR560771248211");
        userEmpAss2.setContract_number("UWKF612197");
        userEmpAss2.setHire_date("13/3/2005");
        userEmpAss2.setPosition("asistent medical");
        userEmpAss2.setUsername("cosmin.gheorghe");
        userEmpAss2.setPassword("EoldP76T[E");

        userDc1 = new User();
        userDc1.setPersonal_identifier("1171129225779");
        userDc1.setFirst_name("Mircea");
        userDc1.setLast_name("POP");
        userDc1.setAddress("Timisoara, Timis");
        userDc1.setPhone_number("0704370970");
        userDc1.setEmail("mircea.pop@bigstring.com");
        userDc1.setIBAN_account("IS16BFRO988480257559");
        userDc1.setContract_number("MVYZ603566");
        userDc1.setHire_date("24/8/1994");
        userDc1.setPosition("medic");
        userDc1.setUsername("mircea.pop");
        userDc1.setPassword("RNT72w3~7j");
    }

    @Test
    public void testAdd() throws Exception {
        userDao.add(user1);
        userDao.add(user2);
        userDao.add(user3);
        userDao.add(user4);
        userDao.add(userEmpAss2);
        userDao.add(userDc1);
    }

    @Test
    public void testDelete() throws Exception {
        userDao.delete(user1.getPersonal_identifier());
        userDao.delete(user2.getPersonal_identifier());
        userDao.delete(user3.getPersonal_identifier());
        userDao.delete(user4.getPersonal_identifier());
        userDao.delete(userEmpAss2.getPersonal_identifier());
        userDao.delete(userDc1.getPersonal_identifier());
    }

    @Test
    public void testGetById() throws Exception {
        User entity = userDao.getById("1200211304601");
        show(entity);
    }

    @Test
    public void testGetAll() throws Exception {
        List<User> users = userDao.getAll();

        for (User entity : users){
            show(entity);
        }
    }

    @Test
    public void testUpdate() throws Exception {
        user1.setPosition("administrator");
        userDao.update(user1);
    }

    private void show(User entity){
        System.out.println("--------------------------------------");
        System.out.println(entity.getPersonal_identifier());
        System.out.println(entity.getFirst_name());
        System.out.println(entity.getLast_name());
        System.out.println(entity.getAddress());
        System.out.println(entity.getPhone_number());
        System.out.println(entity.getEmail());
        System.out.println(entity.getIBAN_account());
        System.out.println(entity.getContract_number());
        System.out.println(entity.getHire_date());
        System.out.println(entity.getPosition());
        System.out.println(entity.getUsername());
        System.out.println(entity.getPassword());
    }
}