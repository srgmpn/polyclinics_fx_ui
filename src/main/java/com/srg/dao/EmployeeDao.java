package com.srg.dao;

import com.srg.model.Employee;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by administrator on 8/1/15.
 */
public interface EmployeeDao {
    List<Employee> search(Employee employee) throws SQLException;
}
