package com.srg.dao;

import com.srg.model.Assistant;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by administrator on 8/2/15.
 */
public interface AssitantDao  {
    List<Assistant> search(Assistant assistant) throws SQLException;
}
