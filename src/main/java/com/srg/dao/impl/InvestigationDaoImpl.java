package com.srg.dao.impl;

import com.srg.model.Investigation;

import java.util.ArrayList;

/**
 * Created by administrator on 7/14/15.
 */
public class InvestigationDaoImpl extends AbstractCRUDImpl<Investigation> {

    public InvestigationDaoImpl(String tableName) {
        super(tableName);
    }

    @Override
    protected ArrayList<String> setValues(Investigation entity) {
        ArrayList<String> values = new ArrayList<>();

        //values.add(String.valueOf(entity.getId()));
        values.add(entity.getName());
        values.add(String.valueOf(entity.getDuration()));
        values.add(String.valueOf(entity.getPrice()));
        values.add(entity.getAbility());
        values.add(String.valueOf(entity.getId_speciality()));

        return values;
    }

    @Override
    protected Investigation setEntityValues(ArrayList<String> values) {

        Investigation investigation = new Investigation();
        investigation.setId(values.get(0));
        investigation.setName(values.get(1));
        investigation.setDuration(values.get(2));
        investigation.setPrice(values.get(3));
        investigation.setAbility(values.get(4));
        investigation.setId_speciality(values.get(5));

        return investigation;
    }

    @Override
    protected String getId(Investigation entity) {
        return entity.getId();
    }
}
