package com.srg.dao.impl;

import com.srg.dao.DoctorDao;
import com.srg.dataaccess.JdbcDataBaseWrapper;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.Doctor;
import com.srg.util.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/14/15.
 */
public class DoctorDaoImpl extends AbstractCRUDImpl<Doctor> implements DoctorDao{

    private JdbcDataBaseWrapper dataBaseWrapper;

    public DoctorDaoImpl(String tableName) {
        super(tableName);
        dataBaseWrapper = JdbcDataBaseWrapperImpl.getInstance();
    }

    @Override
    protected ArrayList<String> setValues(Doctor entity) {
        return entity.getValues();
    }

    @Override
    protected Doctor setEntityValues(ArrayList<String> values) {
        return new Doctor(values);
    }

    @Override
    protected String getId(Doctor entity) {
        return entity.getPersonal_identifier();
    }

    @Override
    public List<Doctor> search(Doctor doctor) throws SQLException {
        String whereClause = "";
        if (doctor.getPersonal_identifier() != null) {
            whereClause += getWhereClause(doctor.getPersonal_identifier().isEmpty(), "personal_identifier", doctor.getPersonal_identifier(), whereClause);
        }
        whereClause += getWhereClause(doctor.getParaph_code().isEmpty(), "paraph_code", doctor.getParaph_code(), whereClause);
        whereClause += getWhereClause(doctor.getScientific_title().isEmpty(), "scientific_title", doctor.getScientific_title(), whereClause);
        whereClause += getWhereClause(doctor.getDidactic_chair().isEmpty(), "didactic_chair", doctor.getDidactic_chair(), whereClause);

        ArrayList<ArrayList<String>> doctorsValues = dataBaseWrapper.getTableContent(Constants.TABLE_DOCTOR, null, (whereClause.isEmpty() ? null : whereClause), null, null);

        ArrayList<Doctor> doctors = new ArrayList<>();
        doctorsValues.stream().forEach((DoctorValues) -> doctors.add(new Doctor(DoctorValues)));

        return doctors;
    }

    private String getWhereClause(boolean isEmpty, String key, String value, String whereClouse){
        return (isEmpty ? "" : getPair(key, value, whereClouse.isEmpty()));
    }

    private String getPair(String key, String value, boolean and){
        return ((!and ? " AND " : "") + key + "=\'" + value + "\'");
    }
}
