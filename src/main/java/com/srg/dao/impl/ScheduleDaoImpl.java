package com.srg.dao.impl;

import com.srg.dao.ScheduleDao;
import com.srg.dataaccess.JdbcDataBaseWrapper;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.Schedule;
import com.srg.util.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/14/15.
 */
public class ScheduleDaoImpl extends AbstractCRUDImpl<Schedule> implements ScheduleDao{

    private JdbcDataBaseWrapper dataBaseWrapper;

    public ScheduleDaoImpl(String tableName) {
        super(tableName);
        dataBaseWrapper = JdbcDataBaseWrapperImpl.getInstance();
    }

    @Override
    protected ArrayList<String> setValues(Schedule entity) {
        return entity.getValues();
    }

    @Override
    protected Schedule setEntityValues(ArrayList<String> values) {
        Schedule schedule = new Schedule(values);
        schedule.setWeekday(getString(schedule.getWeekday()));
        schedule.setSchedule_date(getString(schedule.getSchedule_date()));
        schedule.setLocation(getString(schedule.getLocation()));
        return schedule;
    }

    @Override
    protected String getId(Schedule entity) {
        return entity.getId();
    }

    private String getString(String value){
        return (((value == null) || value.equals("null")) ? "" : value);
    }

    @Override
    public List<Schedule> search(Schedule schedule) throws SQLException {
        String whereClause = "";
        whereClause += getWhereClause(schedule.getTime_slot().isEmpty(), "time_slot", schedule.getTime_slot(), whereClause);
        whereClause += getWhereClause(schedule.getWeekday().isEmpty(), "weekday", schedule.getWeekday(), whereClause);
        whereClause += getWhereClause(schedule.getSchedule_date().isEmpty(), "schedule_date", schedule.getSchedule_date(), whereClause);
        whereClause += getWhereClause(schedule.getLocation().isEmpty(), "location", schedule.getLocation(), whereClause);
        whereClause += getWhereClause(schedule.getId_employee().isEmpty(), "id_employee", schedule.getId_employee(), whereClause);

        ArrayList<ArrayList<String>> schedulesValues = dataBaseWrapper.getTableContent(Constants.TABLE_SCHEDULE, null, (whereClause.isEmpty() ? null : whereClause), null, null);
        ArrayList<Schedule> schedules = new ArrayList<>();

        schedulesValues.stream().forEach((scheduleValues) -> schedules.add(setEntityValues(scheduleValues)));

        return schedules;
    }

    @Override
    public List<Schedule> getSchedulesByEmplyeeID(String emplyeeID) throws SQLException {
        String whereClause = "";
        whereClause += getWhereClause(emplyeeID.isEmpty(), "id_employee", emplyeeID , whereClause);
        ArrayList<ArrayList<String>> schedulesValues = dataBaseWrapper.getTableContent(Constants.TABLE_SCHEDULE, null, (whereClause.isEmpty() ? null : whereClause), null, null);
        ArrayList<Schedule> schedules = new ArrayList<>();
        schedulesValues.stream().forEach((scheduleValues) -> schedules.add(setEntityValues(scheduleValues)));
        return schedules;
    }

    private String getWhereClause(boolean isEmpty, String key, String value, String whereClouse){
        return (isEmpty ? "" : getPair(key, value, whereClouse.isEmpty()));
    }

    private String getPair(String key, String value, boolean and){
        return ((!and ? " AND " : "") + key + "=\'" + value + "\'");
    }
}
