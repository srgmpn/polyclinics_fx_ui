package com.srg.dao.impl;

import com.srg.dao.AssitantDao;
import com.srg.dataaccess.JdbcDataBaseWrapper;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.Assistant;
import com.srg.util.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/14/15.
 */
public class AssistantDaoImpl extends AbstractCRUDImpl<Assistant> implements AssitantDao{

    private JdbcDataBaseWrapper dataBaseWrapper;

    public AssistantDaoImpl(String tableName) {
        super(tableName);
        dataBaseWrapper = JdbcDataBaseWrapperImpl.getInstance();
    }

    @Override
    protected ArrayList<String> setValues(Assistant entity) {
        return entity.getValues();
    }

    @Override
    protected Assistant setEntityValues(ArrayList<String> values) {
        return new Assistant(values);
    }

    @Override
    protected String getId(Assistant entity) {
        return entity.getPersonal_identifier();
    }

    @Override
    public List<Assistant> search(Assistant assistant) throws SQLException {
        String whereClause = "";
        if (assistant.getPersonal_identifier() != null) {
            whereClause += getWhereClause(assistant.getPersonal_identifier().isEmpty(), "personal_identifier", assistant.getPersonal_identifier(), whereClause);
        }
        whereClause += getWhereClause(assistant.getSpeciality().isEmpty(), "speciality",assistant.getSpeciality(), whereClause);
        whereClause += getWhereClause(assistant.getDegree().isEmpty(), "degree", assistant.getDegree(), whereClause);

        ArrayList<ArrayList<String>> assitantsValues = dataBaseWrapper.getTableContent(Constants.TABLE_ASSITANT, null, (whereClause.isEmpty() ? null : whereClause), null, null);

        ArrayList<Assistant> assistants = new ArrayList<>();
        assitantsValues.stream().forEach((assitantValues) -> assistants.add(new Assistant(assitantValues)));

        return assistants;
    }

    private String getWhereClause(boolean isEmpty, String key, String value, String whereClouse){
        return (isEmpty ? "" : getPair(key, value, whereClouse.isEmpty()));
    }

    private String getPair(String key, String value, boolean and){
        return ((!and ? " AND " : "") + key + "=\'" + value + "\'");
    }
}
