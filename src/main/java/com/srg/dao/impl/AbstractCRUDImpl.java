package com.srg.dao.impl;

import com.srg.dao.BasicCRUDDao;
import com.srg.dataaccess.JdbcDataBaseException;
import com.srg.dataaccess.JdbcDataBaseWrapper;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/14/15.
 */
public abstract class AbstractCRUDImpl<T> implements BasicCRUDDao<T>{

    private JdbcDataBaseWrapper dataBaseWrapper;
    private String tableName;
    private String primaryKey;

    public AbstractCRUDImpl(String tableName){
        this.tableName = tableName;

        dataBaseWrapper = JdbcDataBaseWrapperImpl.getInstance();
        try {
            primaryKey = dataBaseWrapper.getTablePrimaryKey(tableName);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int add(T entity) throws SQLException {
        int result = -1;
        String primaryKey = dataBaseWrapper.getTablePrimaryKey(tableName);
        boolean skipPrimaryKey = false;

        if (primaryKey.equalsIgnoreCase("id")){
            skipPrimaryKey = true;
        }

        ArrayList<String> values = setValues(entity);

        try {
            result = dataBaseWrapper.insertValuesIntoTable(tableName, null, values, skipPrimaryKey);
        } catch (JdbcDataBaseException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public void delete(String id) throws SQLException {
        String whereClause = primaryKey + "=" + id;

        try {
            int result = dataBaseWrapper.deleteRecordsFromTable(tableName, null, null, whereClause);
        } catch (JdbcDataBaseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public T getById(String id) throws SQLException {


        String whereClause = primaryKey + "=" + id;
     //   System.out.println(whereClause);
        ArrayList<ArrayList<String>> content = dataBaseWrapper.getTableContent(tableName, null, whereClause, null, null);

        if (!content.isEmpty()){
            T entity = setEntityValues(content.get(0));
            return entity;
        }

        return null;
    }

    @Override
    public List<T> getAll() throws SQLException {

        ArrayList<ArrayList<String>> content = dataBaseWrapper.getTableContent(tableName, null, null, null, null);

        if (!content.isEmpty()){
            List<T> values = new ArrayList<>();
            for (ArrayList<String> entity : content){
                values.add(setEntityValues(entity));
            }

            return values;
        }
        return null;
    }

    @Override
    public void update(T entity) throws SQLException {
        String whereClause = primaryKey + "=" + getId(entity);

        try {
            dataBaseWrapper.updateRecordsIntoTable(tableName, null, setValues(entity), whereClause);
        } catch (JdbcDataBaseException e) {
            e.printStackTrace();
        }
    }

    protected abstract ArrayList<String> setValues(T entity);
    protected abstract T setEntityValues(ArrayList<String> values);
    protected abstract String getId(T entity);
}
