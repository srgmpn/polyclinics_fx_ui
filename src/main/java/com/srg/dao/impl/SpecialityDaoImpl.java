package com.srg.dao.impl;

import com.srg.model.Speciality;

import java.util.ArrayList;

/**
 * Created by administrator on 7/14/15.
 */
public class SpecialityDaoImpl extends AbstractCRUDImpl<Speciality> {

    public SpecialityDaoImpl(String tableName) {
        super(tableName);
    }

    @Override
    protected ArrayList<String> setValues(Speciality entity) {

        ArrayList<String> values = new ArrayList<>();
        //values.add(String.valueOf(entity.getId()));
        values.add(entity.getName());

        return values;
    }

    @Override
    protected Speciality setEntityValues(ArrayList<String> values) {

        Speciality speciality = new Speciality();
        speciality.setId(values.get(0));
        speciality.setName(values.get(1));

        return speciality;
    }

    @Override
    protected String getId(Speciality entity) {
        return entity.getId();
    }
}
