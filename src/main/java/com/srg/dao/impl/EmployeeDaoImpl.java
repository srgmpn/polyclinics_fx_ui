package com.srg.dao.impl;

import com.srg.dao.EmployeeDao;
import com.srg.dataaccess.JdbcDataBaseWrapper;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.Employee;
import com.srg.util.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/14/15.
 */
public class EmployeeDaoImpl extends AbstractCRUDImpl<Employee> implements EmployeeDao{

    private JdbcDataBaseWrapper dataBaseWrapper;

    public EmployeeDaoImpl(String tableName) {
        super(tableName);
        dataBaseWrapper = JdbcDataBaseWrapperImpl.getInstance();
    }

    @Override
    protected ArrayList<String> setValues(Employee employee){
        return employee.getValues();
    }

    @Override
    protected Employee setEntityValues(ArrayList<String> values) {
        return new Employee(values);
    }

    @Override
    protected String getId(Employee entity) {
        return entity.getPersonal_identifier();
    }

    @Override
    public List<Employee> search(Employee employee) throws SQLException {
        String whereClause = "";
        if (employee.getPersonal_identifier() != null) {
            whereClause += getWhereClause(employee.getPersonal_identifier().isEmpty(), "personal_identifier", employee.getPersonal_identifier(), whereClause);
        }
        whereClause += getWhereClause(employee.getSalary().isEmpty(), "salary", employee.getSalary(), whereClause);
        whereClause += getWhereClause(employee.getWorking_hours().isEmpty(), "working_hours", employee.getWorking_hours(), whereClause);

        ArrayList<ArrayList<String>> employeeValues = dataBaseWrapper.getTableContent(Constants.TABLE_EMPLOYEE, null, (whereClause.isEmpty() ? null : whereClause), null, null);

        ArrayList<Employee> employees = new ArrayList<>();
        employeeValues.stream().forEach((employeeValue) -> employees.add(new Employee(employeeValue)));

        return employees;
    }

    private String getWhereClause(boolean isEmpty, String key, String value, String whereClouse){
        return (isEmpty ? "" : getPair(key, value, whereClouse.isEmpty()));
    }

    private String getPair(String key, String value, boolean and){
        return ((!and ? " AND " : "") + key + "=\'" + value + "\'");
    }
}
