package com.srg.dao.impl;

import com.srg.dao.AbilityDao;
import com.srg.dataaccess.JdbcDataBaseWrapper;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.Ability;
import com.srg.util.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/14/15.
 */
public class AbilityDaoImpl extends AbstractCRUDImpl<Ability> implements AbilityDao {

    private JdbcDataBaseWrapper dataBaseWrapper;

    public AbilityDaoImpl(String tableName) {
        super(tableName);
        dataBaseWrapper = JdbcDataBaseWrapperImpl.getInstance();
    }

    @Override
    protected ArrayList<String> setValues(Ability entity) {
        return entity.getValues();
    }

    @Override
    protected Ability setEntityValues(ArrayList<String> values) {
        return new Ability(values);
    }

    @Override
    protected String getId(Ability entity) {
        return entity.getId();
    }

    @Override
    public ArrayList<String> getAbilitiesBySpecialityDoctorID(String specialityDoctorID) throws SQLException {

        String whereClause = "id_speciality_doctor=\'" + specialityDoctorID + "\'";
        //whereClause += " AND name=\'" + abilityName + "\'";
        ArrayList<String> attributes = new ArrayList<>();
        attributes.add("name");

        ArrayList<ArrayList<String>> content = dataBaseWrapper.getTableContent(Constants.TABLE_ABILITY, attributes, whereClause, null, null);

        if (content != null && !content.isEmpty()){
            ArrayList<String> abilities = new ArrayList<>();
            content.stream().forEach((ability) -> abilities.add(ability.get(0)));
            return abilities;
        }
        return null;
    }

    @Override
    public List<Ability> search(Ability ability) throws SQLException {
        String whereClause = "";
        whereClause += getWhereClause(ability.getName().isEmpty(), "name", ability.getName(), whereClause);
        whereClause += getWhereClause(ability.getId_speciality_doctor().isEmpty(), "id_speciality_doctor", ability.getId_speciality_doctor(), whereClause);

        ArrayList<ArrayList<String>> abilitiesValues = dataBaseWrapper.getTableContent(Constants.TABLE_ABILITY, null, (whereClause.isEmpty() ? null : whereClause), null, null);

        ArrayList<Ability> abilities = new ArrayList<>();

        abilitiesValues.stream().forEach((nability) -> abilities.add(new Ability(nability)));

        return abilities;
    }
    private String getWhereClause(boolean isEmpty, String key, String value, String whereClouse){
        return (isEmpty ? "" : getPair(key, value, whereClouse.isEmpty()));
    }

    private String getPair(String key, String value, boolean and){
        return ((!and ? " AND " : "") + key + "=\'" + value + "\'");
    }
}
