package com.srg.dao.impl;

import com.srg.dao.UserDao;
import com.srg.dataaccess.JdbcDataBaseWrapper;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.User;
import com.srg.util.Constants;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by administrator on 7/13/15.
 */
public class UserDaoImpl extends AbstractCRUDImpl<User> implements UserDao {

    private JdbcDataBaseWrapper dataBaseWrapper;

    public UserDaoImpl(String tableName) {
        super(tableName);
        dataBaseWrapper = JdbcDataBaseWrapperImpl.getInstance();
    }

    @Override
    protected ArrayList<String> setValues(User entity) {
        return entity.getValues();
    }

    @Override
    protected User setEntityValues(ArrayList<String> values) {
        return new User(values);
    }

    @Override
    protected String getId(User entity) {
        return entity.getPersonal_identifier();
    }

    /**
     * get position of user
     * @param username
     * @param password
     * @return
     */
    @Override
    public ArrayList<ArrayList<String>> getUserRoles(String username, String password){

        ArrayList<ArrayList<String>> userRoles = null;
        ArrayList<String> attributes = new ArrayList<>();
        attributes.add("position");
        attributes.add("personal_identifier");
        String whereClause = "username=\'" + username + "\' AND password=\'" + password + "\'";
        try {
            userRoles = dataBaseWrapper.getTableContent(Constants.TABLE_USER, attributes, whereClause, null, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userRoles;
    }

    @Override
    public ArrayList<User> search(User user) throws SQLException {
        String whereClause = "";
        whereClause += getWhereClause(user.getPersonal_identifier().isEmpty(), "personal_identifier", user.getPersonal_identifier(), whereClause);
        whereClause += getWhereClause(user.getFirst_name().isEmpty(), "first_name", user.getFirst_name(), whereClause);
        whereClause += getWhereClause(user.getLast_name().isEmpty(), "last_name", user.getLast_name(), whereClause);
        whereClause += getWhereClause(user.getAddress().isEmpty(), "address", user.getAddress(), whereClause);
        whereClause += getWhereClause(user.getPhone_number().isEmpty(), "phone_number", user.getPhone_number(), whereClause);
        whereClause += getWhereClause(user.getEmail().isEmpty(), "email", user.getEmail(), whereClause);
        whereClause += getWhereClause(user.getIBAN_account().isEmpty(), "IBAN_account", user.getIBAN_account(), whereClause);
        whereClause += getWhereClause(user.getContract_number().isEmpty(), "contract_number", user.getContract_number(), whereClause);
        whereClause += getWhereClause(user.getHire_date().isEmpty(), "hire_date", user.getHire_date(), whereClause);
        whereClause += getWhereClause((user.getPosition() == null), "position", user.getPosition(), whereClause);
        whereClause += getWhereClause(user.getUsername().isEmpty(), "username", user.getUsername(), whereClause);
        whereClause += getWhereClause(user.getPassword().isEmpty(), "password", user.getPassword(), whereClause);

        ArrayList<ArrayList<String>> userValues = dataBaseWrapper.getTableContent(Constants.TABLE_USER, null, (whereClause.isEmpty() ? null : whereClause), null, null);
        ArrayList<User> users = new ArrayList<>();
        userValues.stream().forEach((userValue) -> users.add(new User(userValue)));

        return users;
    }

    private String getWhereClause(boolean isEmpty, String key, String value, String whereClouse){
        return (isEmpty ? "" : getPair(key, value, whereClouse.isEmpty()));
    }

    private String getPair(String key, String value, boolean and){
        return ((!and ? " AND " : "") + key + "=\'" + value + "\'");
    }
}
