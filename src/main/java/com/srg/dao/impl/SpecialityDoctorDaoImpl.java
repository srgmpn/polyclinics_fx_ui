package com.srg.dao.impl;

import com.srg.dao.SpecialityDoctorDao;
import com.srg.dataaccess.JdbcDataBaseWrapper;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.SpecialityDoctor;
import com.srg.util.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/14/15.
 */
public class SpecialityDoctorDaoImpl extends AbstractCRUDImpl<SpecialityDoctor> implements SpecialityDoctorDao{

    private JdbcDataBaseWrapper dataBaseWrapper;

    public SpecialityDoctorDaoImpl(String tableName) {
        super(tableName);
        dataBaseWrapper = JdbcDataBaseWrapperImpl.getInstance();
    }

    @Override
    protected ArrayList<String> setValues(SpecialityDoctor entity) {
        return entity.getValues();
    }

    @Override
    protected SpecialityDoctor setEntityValues(ArrayList<String> values) {
        return new SpecialityDoctor(values);
    }

    @Override
    protected String getId(SpecialityDoctor entity) {
        return entity.getId();
    }

    @Override
    public ArrayList<String> getSpecialitiesByDoctorID(String doctorId) throws SQLException {

        String whereClause = "id_doctor=\'" + doctorId + "\'";
        //whereClause += " AND speciality=\'" + specialityName + "\'";
        ArrayList<String> attributes = new ArrayList<>();
        attributes.add("speciality");

        ArrayList<ArrayList<String>> content = dataBaseWrapper.getTableContent(Constants.TABLE_SPECIALITY_DOCTOR, attributes, whereClause, null, null);

        if (content != null && !content.isEmpty()){
            ArrayList<String> specialities = new ArrayList<>();
            content.stream().forEach((speciality) -> specialities.add(speciality.get(0)));
            return specialities;
        }

        return null;

    }

    @Override
    public List<SpecialityDoctor> search(SpecialityDoctor specialityDoctor) throws SQLException {
        String whereClause = "";
        whereClause += getWhereClause(specialityDoctor.getSpeciality().isEmpty(), "speciality", specialityDoctor.getSpeciality(), whereClause);
        whereClause += getWhereClause(specialityDoctor.getDegree().isEmpty(), "degree", specialityDoctor.getDegree(), whereClause);
        whereClause += getWhereClause(specialityDoctor.getId_doctor().isEmpty(), "id_doctor", specialityDoctor.getId_doctor(), whereClause);

        ArrayList<ArrayList<String>> specialityDoctorsValues = dataBaseWrapper.getTableContent(Constants.TABLE_SPECIALITY_DOCTOR, null, (whereClause.isEmpty() ? null : whereClause), null, null);

        ArrayList<SpecialityDoctor> specialityDoctors = new ArrayList<>();

        specialityDoctorsValues.stream().forEach((specialityDoctorValues) -> specialityDoctors.add(new SpecialityDoctor(specialityDoctorValues)));

        return specialityDoctors;
    }

    private String getWhereClause(boolean isEmpty, String key, String value, String whereClouse){
        return (isEmpty ? "" : getPair(key, value, whereClouse.isEmpty()));
    }

    private String getPair(String key, String value, boolean and){
        return ((!and ? " AND " : "") + key + "=\'" + value + "\'");
    }
}
