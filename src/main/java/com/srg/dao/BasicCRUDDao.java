package com.srg.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by administrator on 7/13/15.
 */
public interface BasicCRUDDao<T> {

    int add(T entity) throws SQLException;

    void delete(String id) throws SQLException;

    T getById(String id) throws SQLException;

    List<T> getAll() throws SQLException;

    void update (T entity) throws SQLException;

}
