package com.srg.dao;

import com.srg.dao.impl.*;
import com.srg.util.Constants;

/**
 * Created by administrator on 7/28/15.
 */
public class DaoFactory {

    private DaoFactory(){}

    public static BasicCRUDDao getDao(String modelName) {

        switch (modelName) {
            case Constants.TABLE_USER:
                return new UserDaoImpl(Constants.TABLE_USER);
            case Constants.TABLE_DOCTOR:
                return new DoctorDaoImpl(Constants.TABLE_DOCTOR);
            case Constants.TABLE_ABILITY:
                return new AbilityDaoImpl(Constants.TABLE_ABILITY);
            case Constants.TABLE_ASSITANT:
                return new AssistantDaoImpl(Constants.TABLE_ASSITANT);
            case Constants.TABLE_EMPLOYEE:
                return new EmployeeDaoImpl(Constants.TABLE_EMPLOYEE);
            case Constants.TABLE_INVESTIGATION:
                return new InvestigationDaoImpl(Constants.TABLE_INVESTIGATION);
            case Constants.TABLE_SCHEDULE:
                return new ScheduleDaoImpl(Constants.TABLE_SCHEDULE);
            case Constants.TABLE_SPECTIALITY:
                return new SpecialityDaoImpl(Constants.TABLE_SPECTIALITY);
            case Constants.TABLE_SPECIALITY_DOCTOR:
                return new SpecialityDoctorDaoImpl(Constants.TABLE_SPECIALITY_DOCTOR);
        }
        return null;
    }
}
