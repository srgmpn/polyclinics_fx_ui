package com.srg.dao;

import com.srg.model.SpecialityDoctor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 8/2/15.
 */
public interface SpecialityDoctorDao {

    ArrayList<String> getSpecialitiesByDoctorID(String doctorId) throws SQLException;
    List<SpecialityDoctor> search(SpecialityDoctor specialityDoctor) throws SQLException;

}
