package com.srg.dao;

import com.srg.model.Ability;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 8/3/15.
 */
public interface AbilityDao {
    ArrayList<String> getAbilitiesBySpecialityDoctorID(String specialityDoctorID) throws SQLException;
    List<Ability> search(Ability ability) throws SQLException;
}
