package com.srg.dao;

import com.srg.model.Doctor;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by administrator on 8/2/15.
 */
public interface DoctorDao {
    List<Doctor> search(Doctor doctor) throws SQLException;
}
