package com.srg.dao;

import com.srg.model.Schedule;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by administrator on 8/3/15.
 */
public interface ScheduleDao {
    List<Schedule> search(Schedule schedule) throws SQLException;
    List<Schedule> getSchedulesByEmplyeeID(String emplyeeID) throws SQLException;
}
