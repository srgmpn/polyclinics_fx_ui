package com.srg.dao;

import com.srg.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/30/15.
 */
public interface UserDao {
    /**
     * get user position of user which has username and password
     * equal with specified values
     * @param username - username specified value
     * @param password - password specified value
     * @return - user position ([[0]])
     * @throws SQLException
     */
    public ArrayList<ArrayList<String>> getUserRoles(String username, String password) throws SQLException;

    /**
     * search users in db
     * where condition - not empty user fields
     * @param user - user object where are specified values
     * @return - list of users
     * @throws SQLException
     */
    public List<User> search(User user) throws SQLException;
}
