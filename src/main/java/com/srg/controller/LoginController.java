package com.srg.controller;

import com.srg.core.RBACHandle;
import com.srg.dao.impl.AbstractCRUDImpl;
import com.srg.dao.UserDao;
import com.srg.dao.impl.UserDaoImpl;
import com.srg.model.User;
import com.srg.util.Constants;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by administrator on 7/24/15.
 */
public class LoginController {
    private Stage windows;
    private Scene appScene;
    private UserDao userDao = new UserDaoImpl(Constants.TABLE_USER);

    @FXML private TextField userNameTextField;
    @FXML private PasswordField passwordTextField;
    @FXML private Label errorLabel;

    public void start(){

        if (Constants.DEBUG){
            System.out.println("Starting start loginController ...");
        }

        try{
            appScene = new Scene((Parent) FXMLLoader.load(getClass().getResource(Constants.FXML_LOGIN)));

        } catch (Exception e) {
            e.printStackTrace();
        }

        windows = new Stage();
        windows.setTitle(Constants.LOGIN_NAME_DIALOG);
        windows.setScene(appScene);
        windows.show();
    }

    @FXML
    public void okButtonHandler(ActionEvent event){

        ArrayList<ArrayList<String>> userRoles = null;
        try {
            userRoles = userDao.getUserRoles(userNameTextField.getText(), passwordTextField.getText());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (userRoles.isEmpty()){
            errorMessage(Constants.ERROR_USERNAME_PASSWORD);
        } else {
            //get user role
            String userRole = userRoles.get(0).get(0);
            //get personal identifier of user
            String cnp = userRoles.get(0).get(1);

            if (Arrays.asList(Constants.USERS_ROLES).contains(userRole)){

                //get User dates is DB
                try {
                    RBACHandle.getInstance().setUser(((AbstractCRUDImpl<User>)userDao).getById(cnp));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                //handle user role
                RBACHandle.getInstance().handle(userRole);
                ((Node) event.getSource()).getScene().getWindow().hide();

            } else {
                errorMessage(Constants.ERROR_ACCESS_NOT_ALLOWED_ACCESS_APP);
            }
        }
    }

    @FXML
    protected void cancelButtonHandler(ActionEvent event){
        Platform.exit();
    }

    private void errorMessage(String errorMessage){
        errorLabel.setText(errorMessage);
        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(5), errorLabel);
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.0);
        fadeTransition.play();

        userNameTextField.setText("");
        passwordTextField.setText("");
    }
}
