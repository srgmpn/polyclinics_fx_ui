package com.srg.controller;

import com.srg.controller.admin.tableviewcontroller.*;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.*;
import com.srg.util.Constants;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 7/27/15.
 */
public class AdminController extends AbstractController {



    private Map<String, Object> tabControllersMap;

    @FXML private TabPane dbTabPane;

    public AdminController(){
        super(Constants.ADMIN_MANAGEMENT, true);
        tabControllersMap = new HashMap<>();
    }

    public void start()  {
        //load the controller using reflection
        try {
            scene = new Scene((Parent) FXMLLoader.load(getClass().getResource(Constants.FXML_ADMIN_MANAGEMENT)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.start();
    }

    @FXML
    public void initialize(){

        if (Constants.DEBUG){
            System.out.println("Starting initialize AdminController ...");
        }
        //call super calss initialize method
        super.initialize();

        dbTabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            for (Tab tab : dbTabPane.getTabs()){

                if (tab.getText().equals(newValue.getText()) && !tabControllersMap.isEmpty() &&
                        tabControllersMap.containsKey(newValue.getText())){
                    ((TableViewAbstractController)tabControllersMap.get(newValue.getText())).initialize();
                }
            }
        });


        loadTable(Constants.TABLE_USER);
    }

    /**
     * get All tables name of DB
     * and add to menu Database Management
     * and open default users dates
     */
    @Override
    protected void setOperationMenu(){

        if (Constants.DEBUG){
            System.out.println("Starting set Operation Menu AdminController ...");
        }

        try {
            ArrayList<String> tableNames = JdbcDataBaseWrapperImpl.getInstance().getTableNames();

            tableNames.stream()
                    .forEach((tableName) -> {
                        if (tableName.startsWith(Constants.PREFIX_TABLE_NAME)) {
                            MenuItem menuItem = new MenuItem(tableName.substring(2));
                            menuItem.addEventHandler(EventType.ROOT, getMouseEventHandler());
                            operationMenu.getItems().add(menuItem);
                        }
                    });

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * handle events of operations menu
     * @return EventHandler
     */
    @Override
    protected EventHandler<Event> getMouseEventHandler(){
        return event -> {
            if (event.getSource() instanceof MenuItem){
                Platform.runLater(() -> {

                    String menuEntry = Constants.PREFIX_TABLE_NAME + ((MenuItem) event.getSource()).getText();
                    boolean tabExists = false;

                    for (Tab tab: dbTabPane.getTabs()) {
                        if (tab.getText().equals(menuEntry)) {
                            tabExists = true;
                            dbTabPane.getSelectionModel().select(tab);
                        }
                    }

                    if (!tabExists){
                        loadTable(menuEntry);
                    }
                });
            }
        };
    }

    /**
     * create a new TableView for tableName
     * specified
     * @param tableName
     */
    private void loadTable(String tableName){
        FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(Constants.FXML_TABLE_MANAGEMENT));
        setControllerView(fxmlLoader, tableName);

        Parent view = null;
        try {
            view = (Parent) fxmlLoader.load();
        } catch (IOException ioException) {
            System.out.println("An exception has occured: " + ioException.getMessage());
        }
        Tab tab = new Tab();
        tab.setText(tableName);
        tab.setContent(view);
        dbTabPane.getTabs().add(tab);
        dbTabPane.getSelectionModel().select(tab);
    }

    /**
     * create and set a dinamic new FXMLController in dependecy of table name
     * @param fxmlLoader - fxmlLoader new object
     * @param tableName - table name
     */
    private void setControllerView(FXMLLoader fxmlLoader, String tableName){
        switch (tableName){
            case Constants.TABLE_USER:
                TableViewAbstractController<User> controllerTableViewUser = new TableViewUserController();
                fxmlLoader.setController(controllerTableViewUser);
                tabControllersMap.put(Constants.TABLE_USER, controllerTableViewUser);
                break;

            case Constants.TABLE_DOCTOR:
                TableViewAbstractController<Doctor> controllerTableViewDoctor = new TableViewDoctorController();
                fxmlLoader.setController(controllerTableViewDoctor);
                tabControllersMap.put(Constants.TABLE_DOCTOR, controllerTableViewDoctor);
                break;

            case Constants.TABLE_ABILITY:
                TableViewAbstractController<Ability> controllerTableViewAbility = new TableViewAbilityController();
                fxmlLoader.setController(controllerTableViewAbility);
                tabControllersMap.put(Constants.TABLE_ABILITY, controllerTableViewAbility);
                break;

            case Constants.TABLE_ASSITANT:
                TableViewAbstractController<Assistant> controllerTableViewAssistant = new TableViewAssistantController();
                fxmlLoader.setController(controllerTableViewAssistant);
                tabControllersMap.put(Constants.TABLE_ASSITANT, controllerTableViewAssistant);
                break;

            case Constants.TABLE_EMPLOYEE:
                TableViewAbstractController<Employee> controllerTableViewEmployee = new TableViewEmployeeController();
                fxmlLoader.setController(controllerTableViewEmployee);
                tabControllersMap.put(Constants.TABLE_EMPLOYEE, controllerTableViewEmployee);
                break;
/*
            case Constants.TABLE_INVESTIGATION:
                TableViewAbstractController<Investigation> controllerTableViewInvestigation = new TableViewInvestigationController();
                fxmlLoader.setController(controllerTableViewInvestigation);
                tabControllersMap.put(Constants.TABLE_INVESTIGATION, controllerTableViewInvestigation);
                break;*/

            case Constants.TABLE_SCHEDULE:
                TableViewAbstractController<Schedule> controllerTableViewSchedule = new TableViewScheduleController();
                fxmlLoader.setController(controllerTableViewSchedule);
                tabControllersMap.put(Constants.TABLE_SCHEDULE, controllerTableViewSchedule);
                break;

            case Constants.TABLE_SPECIALITY_DOCTOR:
                TableViewAbstractController<SpecialityDoctor> controllerTableViewSpecialityDoctor = new TableViewSpecialityDoctorController();
                fxmlLoader.setController(controllerTableViewSpecialityDoctor);
                tabControllersMap.put(Constants.TABLE_SPECIALITY_DOCTOR, controllerTableViewSpecialityDoctor);
                break;
/*
            case Constants.TABLE_SPECTIALITY:
                TableViewAbstractController<Speciality> controllerTableViewSpeciality = new TableViewSpecialityController();
                fxmlLoader.setController(controllerTableViewSpeciality);
                tabControllersMap.put(Constants.TABLE_SPECTIALITY, controllerTableViewSpeciality);
                break;*/

            default:
                if (Constants.DEBUG){
                    System.out.println("ERROR in selection table view");
                }
        }
    }
}
