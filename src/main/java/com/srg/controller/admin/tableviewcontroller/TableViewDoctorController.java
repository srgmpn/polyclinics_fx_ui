package com.srg.controller.admin.tableviewcontroller;

import com.srg.controller.DialogController;
import com.srg.dao.BasicCRUDDao;
import com.srg.dao.DaoFactory;
import com.srg.dao.DoctorDao;
import com.srg.model.Doctor;
import com.srg.util.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/29/15.
 */
public class TableViewDoctorController extends TableViewAbstractUsersController<Doctor> {

    private BasicCRUDDao<Doctor> daoDoctor;
    private Doctor doctor;

    public TableViewDoctorController() {
        super(Constants.TABLE_DOCTOR);
        daoDoctor = DaoFactory.getDao(Constants.TABLE_DOCTOR);
    }

    @FXML
    public void initialize(){
        super.initialize();
        tableView.getSelectionModel().selectedItemProperty().addListener(event -> {
            doctor = tableView.getSelectionModel().getSelectedItem();
            if (doctor != null){
                //set PK readOnly
                setIdReadOnly((infChecckBox.isSelected() ? false : true));

                fillFieldsOnSelectionIsTableViewCell();
            }
        });
    }

    @Override
    protected void populateView(List<Doctor> doctorsList) {
        ObservableList<Doctor> doctors = FXCollections.observableArrayList();

        if (doctorsList == null || doctorsList.isEmpty()) {
            try {
                doctors.addAll(daoDoctor.getAll());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            doctors.addAll(doctorsList);
        }
        tableView.setItems(doctors);
    }

    @Override
    protected Control getControlHandler(String attribute) {
        return super.getControlHandlerUser(attribute, Constants.ROLE3);
    }

    @Override
    public void insertButtonHandler() {
        if (!super.checkAllFieldsCompletion()){
            //System.out.println("ERROR (insertButtonHandler (doctor)) -> Complete all fields !");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_COMPLETE_ALL_FIELDS);
            dialogController.start();
            return;
        }

        if (!checkValidPersonalIdentifier(getDoctorFieldsValuesList().get(0))){
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_LENGHT_OF_PERSONAL_IDENTIFIER_ISNOT_13);
            dialogController.start();
            return;
        }
        Doctor newDoctor = new Doctor(getDoctorFieldsValuesList());
        try {
            if (checkDulicateValue(newDoctor.getPersonal_identifier())) {
                //System.out.println("ERROR (insertButtonHandler (doctor)) -> Duplicate Value \"Personal Identifier\"");
                DialogController dialogController = new DialogController();
                dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_DUPLICATE_VALUE);
                dialogController.start();
                return;
            }

            if (Constants.DEBUG){
                System.out.println("Doctor inserted");
            }

            if (infChecckBox.isSelected()){
                super.userAndEmployeeDaoInsert(5, 4);
            }
            //add new Asstant
            daoDoctor.add(newDoctor);
            //update tableview
            updateAndClearView(tableView.getItems().size() - 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if ((tableViewSelectedIndex == -1) || (!checkAllFieldsCompletion())){
            //System.out.println("ERROR (updateButtonHandler (doctor)) -> Are empty fields or wasn't select a value for update");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE_OR_ARE_EMPTY_FIELDS);
            dialogController.start();
            return;
        }
        Doctor updateDoctor = new Doctor(getDoctorFieldsValuesList());
        try {
            if (Constants.DEBUG){
                System.out.println("Doctor updated");
            }

            if (infChecckBox.isSelected()){
                super.userAndEmployeeDaoUpdate(5, 4);
            }
            //add new Asstant
            daoDoctor.update(updateDoctor);
            //update tableview
            updateAndClearView(tableViewSelectedIndex);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteButtonHandler() {
        super.deleteButtonHandleUser(doctor.getPersonal_identifier());
    }

    @Override
    public void searchButtonHandler() {
        ArrayList<String> values = getDoctorFieldsValuesList();

        try {
            if (values == null || values.isEmpty()){
                populateView(null);

            } else {
                List<Doctor> doctors = ((DoctorDao)daoDoctor).search(new Doctor(values));
                populateView(doctors);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void infChecckBoxHandler() {
        super.infChecckBoxHandlerUser(Constants.TABLE_USER, Constants.TABLE_EMPLOYEE);
    }

    @Override
    public void clearButtonHandler() {
        super.clearButtonHandleUser();
    }

    private void fillFieldsOnSelectionIsTableViewCell(){
        try {
            employee = daoEmployee.getById(doctor.getPersonal_identifier());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (!infChecckBox.isSelected()) {
            ((ComboBox<String>) attributesControls.get(0)).setValue(employee.toString());
            setDoctorFieldsValues(1, 2, 3);
            return;
        }

        try {
            user = userDao.getById(doctor.getPersonal_identifier());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //set values in controls
        super.setUserFieldsValues(user, 5);
        super.setEmployeeFieldsValues(employee, attributesControls.size()-5, attributesControls.size()-4);
        setDoctorFieldsValues(attributesControls.size()-3, attributesControls.size()-2, attributesControls.size()-1);
    }

    private ArrayList<String> getDoctorFieldsValuesList(){
        if (infChecckBox.isSelected()){
            int size = super.getFieldsValues().size();
            return super.getPersonalValuesListIFISCheckFullInformation(0, size-3, size-2, size-1);
        }

        return super.getPersonalValuesList();
    }

    private void setDoctorFieldsValues(int index1, int index2, int index3){
        ((TextField) attributesControls.get(index1)).setText(doctor.getParaph_code());
        ((TextField) attributesControls.get(index2)).setText(doctor.getScientific_title());
        ((TextField) attributesControls.get(index3)).setText(doctor.getDidactic_chair());
    }

    /**
     * check if exist in assistant table assistant with
     * specified personal identifier
     * @param cnp
     * @return
     * @throws SQLException
     */
    private boolean checkDulicateValue(String cnp) throws SQLException {
        return ((daoDoctor.getById(cnp) != null) ? true : false);
    }
}
