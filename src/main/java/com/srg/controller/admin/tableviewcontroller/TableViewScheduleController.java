package com.srg.controller.admin.tableviewcontroller;

import com.srg.controller.DialogController;
import com.srg.dao.BasicCRUDDao;
import com.srg.dao.ScheduleDao;
import com.srg.dao.impl.EmployeeDaoImpl;
import com.srg.dao.impl.ScheduleDaoImpl;
import com.srg.model.Employee;
import com.srg.model.Schedule;
import com.srg.util.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by administrator on 7/29/15.
 */
public class TableViewScheduleController extends TableViewAbstractController<Schedule> {

    private BasicCRUDDao<Employee> daoEmployee;
    private BasicCRUDDao<Schedule> scheduleDao;
    private Schedule schedule;

    public TableViewScheduleController() {
        super(Constants.TABLE_SCHEDULE);
        daoEmployee = new EmployeeDaoImpl(Constants.TABLE_EMPLOYEE);
        scheduleDao = new ScheduleDaoImpl(Constants.TABLE_SCHEDULE);
    }

    @FXML
    public void initialize(){
        super.initialize();
        infChecckBox.setDisable(true);
        setIdReadOnly(false);

        tableView.getSelectionModel().selectedItemProperty().addListener(event -> {
            schedule = tableView.getSelectionModel().getSelectedItem();
            if (schedule != null){
                ((TextField) attributesControls.get(0)).setText(schedule.getId());
                ((TextField) attributesControls.get(1)).setText(schedule.getTime_slot());
                ((TextField) attributesControls.get(2)).setText(schedule.getWeekday());
                ((TextField) attributesControls.get(3)).setText(schedule.getSchedule_date());
                ((TextField) attributesControls.get(4)).setText(schedule.getLocation());
                try {
                    Employee employee = daoEmployee.getById(schedule.getId_employee());
                    ((ComboBox<String>)attributesControls.get(5)).setValue(employee.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    protected void populateView(List<Schedule> scheduleList) {
        ObservableList<Schedule> schedules = FXCollections.observableArrayList();

        if (scheduleList == null || scheduleList.isEmpty()){
            try {
                schedules.addAll(scheduleDao.getAll());

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            schedules.addAll(scheduleList);
        }
        tableView.setItems(schedules);
    }

    @Override
    protected Control getControlHandler(String attribute) {
        if (attribute.equals("id_employee")){
            ComboBox<String> attributeComboBox = getComboboxControl();
            try {
                List<Employee> employees = daoEmployee.getAll();
                employees.stream().forEach(
                        (employee) -> attributeComboBox.getItems().add(employee.toString()));

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return attributeComboBox;
        }

        return getTextFieldControl(attribute);
    }

    @Override
    public void insertButtonHandler() {
        if (!checkAllFieldsCompletion()){
            //System.out.println("ERROR (insertButtonHandler (schedule)) -> Complete all fields !");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_COMPLETE_ALL_FIELDS);
            dialogController.start();
            return;
        }

        Schedule newSchedule = new Schedule(getScheduleFieldsValuesList());

        if (Constants.DEBUG){
            System.out.println("Schedule inserted");
        }

        try {
            scheduleDao.add(newSchedule);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //update tableview
        updateView(tableView.getItems().size() - 1);
        clearButtonHandler();
    }

    @Override
    public void updateButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if ((tableViewSelectedIndex == -1) || (!checkAllFieldsCompletion())){
            //System.out.println("ERROR (updateButtonHandler (schedule)) -> Are empty fields or wasn't select a value for update");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE_OR_ARE_EMPTY_FIELDS);
            dialogController.start();
            return;
        }

        Schedule updateSchedule = new Schedule(getScheduleFieldsValuesList());

        if (Constants.DEBUG){
            System.out.println("Schedule update");
        }

        try {
            scheduleDao.update(updateSchedule);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //update tableview
        updateView(tableView.getItems().size() - 1);
        clearButtonHandler();
    }

    @Override
    public void deleteButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if (tableViewSelectedIndex == -1){
            ///System.out.println("ERROR (deleteButtonHandler (schedule)) -> Select a Schedule ");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE);
            dialogController.start();
            return;
        }

        try {
            scheduleDao.delete(schedule.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (Constants.DEBUG){
            System.out.println("Schedule deleted");
        }
        //update view
        updateView(0);
        clearButtonHandler();
    }

    @Override
    public void searchButtonHandler() {
        ArrayList<String> values = getScheduleFieldsValuesList();

        if (values == null || values.isEmpty()){
            populateView(null);

        } else {
            try {
                List<Schedule> schedules =
                        ((ScheduleDao)scheduleDao)
                                .search(new Schedule(values));

                populateView(schedules);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void infChecckBoxHandler() {

    }

    @Override
    public void clearButtonHandler() {
        super.clearButtonHandler();
    }

    protected boolean checkAllFieldsCompletion() {
        if (getValidTextFieldControl(1)){
            return false;
        }

        if (getValidTextFieldControl(2) && getValidTextFieldControl(3))
            return false;

        if (getValidComboboxControl(5)){
            return false;
        }

        return true;
    }

    private boolean getValidTextFieldControl(int index){
        return (((TextField)attributesControls.get(index)).getText() == null ||
                ((TextField)attributesControls.get(index)).getText() .isEmpty());
    }

    private boolean getValidComboboxControl(int index){
        return ((((ComboBox<String>) attributesControls.get(index)).getValue() == null) ||
                (((ComboBox<String>) attributesControls.get(index)).getValue().isEmpty()));
    }

    private ArrayList<String> getScheduleFieldsValuesList(){
        ArrayList<String> values = getFieldsValues();

        if (values == null || values.isEmpty()){
            return null;
        }

        if ((values.get(5) != null) && values.get(5).contains("/")) {
            String cnp = new StringTokenizer(values.get(5), "/").nextToken().trim();
            values.set(5, cnp);
        }
        return values;
    }
}
