package com.srg.controller.admin.tableviewcontroller;

import com.srg.controller.DialogController;
import com.srg.dao.BasicCRUDDao;
import com.srg.dao.DaoFactory;
import com.srg.dao.SpecialityDoctorDao;
import com.srg.model.Doctor;
import com.srg.model.SpecialityDoctor;
import com.srg.util.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by administrator on 7/29/15.
 */
public class TableViewSpecialityDoctorController extends TableViewAbstractController<SpecialityDoctor> {

    private BasicCRUDDao<Doctor> daoDoctor;
    private BasicCRUDDao<SpecialityDoctor> specialityDoctorDao;
    private SpecialityDoctor specialityDoctor;

    public TableViewSpecialityDoctorController() {
        super(Constants.TABLE_SPECIALITY_DOCTOR);
        daoDoctor = DaoFactory.getDao(Constants.TABLE_DOCTOR);
        specialityDoctorDao = DaoFactory.getDao(Constants.TABLE_SPECIALITY_DOCTOR);
    }

    @FXML
    public void initialize(){
        super.initialize();

        //set PK readOnly
        setIdReadOnly(false);
        infChecckBox.setDisable(true);

        tableView.getSelectionModel().selectedItemProperty().addListener(event -> {

            specialityDoctor = tableView.getSelectionModel().getSelectedItem();
            if (specialityDoctor != null){
                ((TextField) attributesControls.get(0)).setText(specialityDoctor.getId());
                ((TextField) attributesControls.get(1)).setText(specialityDoctor.getSpeciality());
                ((TextField) attributesControls.get(2)).setText(specialityDoctor.getDegree());

                try {
                    Doctor doctor = daoDoctor.getById(specialityDoctor.getId_doctor());
                    ((ComboBox<String>)attributesControls.get(3)).setValue(doctor.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void populateView(List<SpecialityDoctor> specialityDoctorList) {
        ObservableList<SpecialityDoctor> specialityDoctors = FXCollections.observableArrayList();

        if (specialityDoctorList == null || specialityDoctorList.isEmpty()) {

            try {
                specialityDoctors.addAll(specialityDoctorDao.getAll());

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            specialityDoctors.addAll(specialityDoctorList);
        }
        tableView.setItems(specialityDoctors);
    }

    @Override
    protected Control getControlHandler(String attribute) {
        if (attribute.equals("id_doctor")){
            ComboBox<String> attributeComboBox = getComboboxControl();
            try {
                List<Doctor> doctors = daoDoctor.getAll();
                doctors.stream().forEach(
                        (doctor) -> {attributeComboBox.getItems().add(doctor.toString());});

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return attributeComboBox;
        }
        return getTextFieldControl(attribute);
    }

    @Override
    public void insertButtonHandler() {

        if (!checkAllFieldsCompletion()){
            //System.out.println("ERROR (insertButtonHandler (specialityDoctor)) -> Complete all fields !");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_COMPLETE_ALL_FIELDS);
            dialogController.start();
            return;
        }

        SpecialityDoctor newSpecialityDoctor = new SpecialityDoctor(getSpecialityDoctorFieldsValuesList());

        if (checkDuplicateSpecialityOfDoctor(newSpecialityDoctor)){
            //System.out.println("Speciality exist in list of doctor specialities ");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_DUPLICATE_VALUE);
            dialogController.start();
            return;
        }

        if (Constants.DEBUG){
            System.out.println("SpecialityDoctor inserted");
        }

        try {
            specialityDoctorDao.add(newSpecialityDoctor);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //update tableview
        updateView(tableView.getItems().size() - 1);
        clearButtonHandler();

    }

    @Override
    public void updateButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if ((tableViewSelectedIndex == -1) || (!checkAllFieldsCompletion())){
            //System.out.println("ERROR (updateButtonHandler (specialitydoctor)) -> Are empty fields or wasn't select a value for update");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE_OR_ARE_EMPTY_FIELDS);
            dialogController.start();
            return;
        }

        SpecialityDoctor updateSpecialityDoctor = new SpecialityDoctor(getSpecialityDoctorFieldsValuesList());

        if (checkDuplicateSpecialityOfDoctor(updateSpecialityDoctor)){
            //System.out.println("Speciality exist in list of doctor specialities ");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_DUPLICATE_VALUE);
            dialogController.start();
            return;
        }

        if (Constants.DEBUG){
            System.out.println("SpecialityDoctor updated");
        }

        try {
            specialityDoctorDao.update(updateSpecialityDoctor);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //update tableview
        updateView(tableView.getItems().size() - 1);
        clearButtonHandler();
    }

    @Override
    public void deleteButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if (tableViewSelectedIndex == -1){
            //System.out.println("ERROR (deleteButtonHandler (specialityDoctor)) -> Select an SpecialityDoctor ");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE);
            dialogController.start();
            return;
        }

        if (Constants.DEBUG){
            System.out.println("SpecialityDoctor deleted");
        }

        try {
            specialityDoctorDao.delete(specialityDoctor.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //update view
        updateView(0);
        clearButtonHandler();
    }

    @Override
    public void searchButtonHandler() {
        ArrayList<String> values = getSpecialityDoctorFieldsValuesList();

        if (values == null || values.isEmpty()){
            populateView(null);

        } else {
            try {
                List<SpecialityDoctor> specialityDoctors =
                        ((SpecialityDoctorDao)specialityDoctorDao)
                                                            .search(new SpecialityDoctor(values));

                populateView(specialityDoctors);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void infChecckBoxHandler() {

    }

    @Override
    public void clearButtonHandler() {
        super.clearButtonHandler();
    }

    private ArrayList<String> getSpecialityDoctorFieldsValuesList(){
        ArrayList<String> values = getFieldsValues();

        if (values == null || values.isEmpty()){
            return null;
        }

        if ((values.get(3) != null) && values.get(3).contains("/")) {
            String cnp = new StringTokenizer(values.get(3), "/").nextToken().trim();
            values.set(3, cnp);
        }
        return values;
    }

    private boolean checkDuplicateSpecialityOfDoctor(SpecialityDoctor newSpecialityDoctor){

        try {
            ArrayList<String> specialitiesList =
                    ((SpecialityDoctorDao) specialityDoctorDao)
                                .getSpecialitiesByDoctorID(newSpecialityDoctor.getId_doctor());
            System.out.println(specialitiesList);

            if (specialitiesList != null && !specialitiesList.isEmpty()){
                return specialitiesList.contains(newSpecialityDoctor.getSpeciality());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }

    protected boolean checkAllFieldsCompletion() {
        int k = 0;
        for (Control control : attributesControls){
            k++;
            String value = null;
            if (control instanceof ComboBox) {
                value = ((ComboBox<String>) control).getValue();
            } else {

                value = ((k == 1) ? "checked" : ((TextField) control).getText());
            }

            if (value == null || value.isEmpty()){
                return false;
            }
        }
        return true;
    }
}
