package com.srg.controller.admin.tableviewcontroller;

import com.srg.controller.DialogController;
import com.srg.dao.EmployeeDao;
import com.srg.model.Employee;
import com.srg.model.User;
import com.srg.util.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/29/15.
 */
public class TableViewEmployeeController extends TableViewAbstractUsersController<Employee> {

    public TableViewEmployeeController() {
        super(Constants.TABLE_EMPLOYEE);
    }

    @FXML
    public void initialize(){
        super.initialize();

        tableView.getSelectionModel().selectedItemProperty().addListener(event -> {

            employee = tableView.getSelectionModel().getSelectedItem();
            if (employee != null) {
                //set PK readOnly
                setIdReadOnly((infChecckBox.isSelected() ? false : true));
                //fill fields
                fillFieldsOnSelectionIsTableViewCell();
            }
        });
    }

    @Override
    protected void populateView(List<Employee> employeeList) {
        ObservableList<Employee> employees = FXCollections.observableArrayList();

        if (employeeList == null || employeeList.isEmpty()) {
            try {
                employees.addAll(daoEmployee.getAll());

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            employees.addAll(employeeList);
        }
        tableView.setItems(employees);
    }

    @Override
    protected Control getControlHandler(String attribute) {

        if (attribute.equals("personal_identifier") && !infChecckBox.isSelected()){
            ComboBox<String> attributeComboBox = getComboboxControl();
            try {
                List<User> users = userDao.getAll();
                users.stream().forEach(
                        (user) ->{
                            if (!user.getPosition().equals(Constants.ROLE1) && !user.getPosition().equals(Constants.ROLE2)) {
                                attributeComboBox.getItems().add(user.toString());
                            }
                        });

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return attributeComboBox;
        }
        //get Combobox if is field position
        if (attribute.equals("position")) {
            return getDefaultUserRoleComboboxControl(Constants.ROLE3, Constants.ROLE4, Constants.ROLE5, Constants.ROLE6, Constants.ROLE7);
        }

        return getTextFieldControl(attribute);
    }

    @Override
    public void insertButtonHandler() {
        //check if fields are not empty
        if (!checkAllFieldsCompletion()) {
            //System.out.println("ERROR (insertButtonHandler (employee)) -> Complete all fields !");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_COMPLETE_ALL_FIELDS);
            dialogController.start();
            return;
        }

        if (!checkValidPersonalIdentifier(getEmployeeFieldsValues().get(0))){
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_LENGHT_OF_PERSONAL_IDENTIFIER_ISNOT_13);
            dialogController.start();
            return;
        }

        Employee newEmployee = new Employee(getEmployeeFieldsValues());
        try {
            if (checkDulicateValue(newEmployee.getPersonal_identifier())) {
                //System.out.println("ERROR (insertButtonHandler (employee)) -> Duplicate Value \"Personal Identifier\"");
                DialogController dialogController = new DialogController();
                dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_DUPLICATE_VALUE);
                dialogController.start();
                return;
            }

            if (Constants.DEBUG){
                System.out.println("Employee inserted");
            }
            if (infChecckBox.isSelected()) {
                //add new user
                userDao.add(new User(super.getUserFieldsValuesList(2)));
            }
            //add new Employee
            daoEmployee.add(newEmployee);
            //update tableview
            updateAndClearView(tableView.getItems().size() - 1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if ((tableViewSelectedIndex == -1) || (!checkAllFieldsCompletion())){
            //System.out.println("ERROR (updateButtonHandler (employee)) -> Are empty fields or was not select a value for update");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE_OR_ARE_EMPTY_FIELDS);
            dialogController.start();
            return;
        }

        if (!checkValidPersonalIdentifier(getEmployeeFieldsValues().get(0))){
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_LENGHT_OF_PERSONAL_IDENTIFIER_ISNOT_13);
            dialogController.start();
            return;
        }

        Employee updateEmployee = new Employee(getEmployeeFieldsValues());
        try {
            if (Constants.DEBUG){
                System.out.println("Employee updated");
            }
            if (infChecckBox.isSelected()) {
                //update user
                userDao.update(new User(super.getUserFieldsValuesList(2)));
            }
            //update employee
            daoEmployee.update(updateEmployee);
            //update tableview
            updateAndClearView(tableViewSelectedIndex);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteButtonHandler() {
        super.deleteButtonHandleUser(employee.getPersonal_identifier());
    }

    @Override
    public void searchButtonHandler() {
        try {
        ArrayList<String> values = getEmployeeFieldsValues();

        if (values == null || values.isEmpty()){
            populateView(null);

        } else {
            List<Employee> employees = ((EmployeeDao)daoEmployee).search(new Employee(values));
            populateView(employees);
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void infChecckBoxHandler() {
        super.infChecckBoxHandlerUser(Constants.TABLE_USER);
    }

    @Override
    public void clearButtonHandler() {
        super.clearButtonHandleUser();
    }

    private void fillFieldsOnSelectionIsTableViewCell(){
        User user = null;
        try {
            user = userDao.getById(employee.getPersonal_identifier());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (!infChecckBox.isSelected()){
            ((ComboBox<String>)attributesControls.get(0)).setValue(user.toString());
            super.setEmployeeFieldsValues(employee, 1, 2);
            return;
        }

        //set values in controls
        super.setUserFieldsValues(user, 2);
        super.setEmployeeFieldsValues(employee, attributesControls.size()-2, attributesControls.size()-1);
    }

    /**
     * get Employee values
     * @return
     */
    private ArrayList<String> getEmployeeFieldsValues(){
        if (infChecckBox.isSelected()){
            return super.getEmployeeFieldsValuesList(2, 1);
        }
        return super.getPersonalValuesList();
    }

    /**
     * check if exist in employee table employee with
     * specified personal identifier
     * @param cnp
     * @return
     * @throws SQLException
     */
    private boolean checkDulicateValue(String cnp) throws SQLException {
        return ((daoEmployee.getById(cnp) != null) ? true : false);
    }
}
