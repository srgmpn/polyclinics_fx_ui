package com.srg.controller.admin.tableviewcontroller;

import com.srg.dao.BasicCRUDDao;
import com.srg.dao.DaoFactory;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.User;
import com.srg.util.Constants;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by administrator on 7/29/15.
 */
public abstract class TableViewAbstractController<T> {

    private String tableName;

    protected Map<String, String> usersMapPosition;
    protected BasicCRUDDao<User> userDao;

    @FXML protected TableView<T> tableView;

    @FXML protected VBox itemParams;

    @FXML protected CheckBox infChecckBox;

    @FXML private Button insertButton;
    @FXML private Button updateButton;
    @FXML private Button deleteButton;
    @FXML private Button searchButton;
    @FXML private Button clearButton;

    protected ArrayList<Control> attributesControls;

    protected TableViewAbstractController(String tableName){
        this.tableName = tableName;
        userDao = DaoFactory.getDao(Constants.TABLE_USER);
        usersMapPosition = new HashMap<>();
    }

    @FXML
    public void initialize(){

        infChecckBox.setSelected(false);

        //set user position map
        setUsersMapPosition();
        //create tableView NameColumns and polulate view
        setTableViewNameColumns();
        //create all editable fields
        setDefaultFieldsTable(false, null);
        //set buttons properties
        btnsSetOnActionInitialize();
        setButtonsEfects();

        //set Checkbox onAction
        infChecckBox.setOnAction(event -> infChecckBoxHandler());

        if (Constants.DEBUG) {
            System.out.println("finishing initialize " + tableName + "...");
        }
    }

    /**
     * get is DB name columns of table
     * and add Columns name to table view
     */
    private void setTableViewNameColumns(){
        try {
            tableView.setItems(null);
            if (tableView.getColumns() != null){
                tableView.getColumns().clear();
            }
            tableView.setEditable(true);

            attributesControls = new ArrayList<>();
            itemParams.getChildren().clear();
            ArrayList<String> attributes = JdbcDataBaseWrapperImpl.getInstance().getTableColumns(tableName);

            for (String attribute : attributes){
                TableColumn<T, String> column = new TableColumn<>(attribute);
                column.setMinWidth(tableView.getPrefWidth() / attributes.size());
                column.setCellValueFactory(new PropertyValueFactory<>(attribute));
                tableView.getColumns().add(column);
            }

            //populate TableView
            populateView(null);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * create all labels and fields of current table
     * if is full Information true add and fields of parent table
     * @param isFullInformation
     * @param parentsTablesName
     */
    protected void setDefaultFieldsTable(boolean isFullInformation, String ...parentsTablesName){

        attributesControls = new ArrayList<>();
        itemParams.getChildren().clear();

        for (String attribute : getAttributesList(isFullInformation, parentsTablesName)){
            Control control = getControlHandler(attribute);
            attributesControls.add(control);
            addNewControlPair(attribute, control);
        }
    }

    /**
     * get Attributes list without dublicate
     * @param isFullInformation - if true exist parent table
     * @param parentsTablesName - name parent table
     * @return
     */
    private ArrayList<String> getAttributesList(boolean isFullInformation, String ...parentsTablesName){
        ArrayList<String> attributes = new ArrayList<>();
        try {
            if (!isFullInformation){
                attributes.addAll(JdbcDataBaseWrapperImpl.getInstance().getTableColumns(tableName));
                return attributes;
            }
            attributes.addAll(getParentTableAttributes(parentsTablesName));

            for (String attribute : JdbcDataBaseWrapperImpl.getInstance().getTableColumns(tableName)) {
                if (!attributes.contains(attribute)) {
                    attributes.add(attribute);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return attributes;
    }

    private ArrayList<String> getParentTableAttributes(String ...parentsTablesName){
        ArrayList<String> parentsAttributes = new ArrayList<>();
        try {
            for (String parentTableName : parentsTablesName) {
                ArrayList<String> parentAttributes = JdbcDataBaseWrapperImpl.getInstance().getTableColumns(parentTableName);
                for (String attribute : parentAttributes) {
                    if (!parentsAttributes.contains(attribute)){
                        parentsAttributes.add(attribute);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return parentsAttributes;
    }

    /**
     * add a Label and a control
     * Combobox of TextField to HBox contrainer
     * and add to itemsParams container
     * @param attributeName
     * @param control
     */
    private void addNewControlPair(String attributeName, Control control){
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
        hBox.getChildren().addAll( getNewLabel(attributeName), control);
        itemParams.getChildren().add(hBox);
    }

    /**
     * add buttons properties: image, tooltip and set action
     */
    private void btnsSetOnActionInitialize(){
        insertButton.setOnAction(event -> insertButtonHandler());
        updateButton.setOnAction(event -> updateButtonHandler());
        deleteButton.setOnAction(event -> deleteButtonHandler());
        searchButton.setOnAction(event -> searchButtonHandler());
        clearButton.setOnAction(event -> clearButtonHandler());
    }

    /**
     * buttons effects
     */
    private void setButtonsEfects(){
        //set buttons efects
        final FadeTransition fadeTransition = new FadeTransition(Duration.seconds(1));
        fadeTransition.setFromValue(1.0f);
        fadeTransition.setToValue(0.8f);
        fadeTransition.setCycleCount(Timeline.INDEFINITE);
        fadeTransition.setAutoReverse(true);
        final ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(1));
        scaleTransition.setByX(0.1f);
        scaleTransition.setByY(0.1f);
        scaleTransition.setCycleCount(Timeline.INDEFINITE);
        scaleTransition.setAutoReverse(true);
        final ParallelTransition parallelTransition = new ParallelTransition(fadeTransition, scaleTransition);

        ArrayList<Button> buttonList = new ArrayList<>();
        buttonList.add(insertButton);
        buttonList.add(updateButton);
        buttonList.add(deleteButton);
        buttonList.add(searchButton);
        buttonList.add(clearButton);

        buttonList.stream().map((currentButton) -> {
            currentButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent event) -> {
                currentButton.setEffect(new DropShadow());
                fadeTransition.setNode(currentButton);
                scaleTransition.setNode(currentButton);
                parallelTransition.play();
            });
            return currentButton;
        }).forEach((currentButton) -> {
            currentButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent event) -> {
                currentButton.setEffect(null);
                parallelTransition.jumpTo(Duration.ZERO);
                fadeTransition.setNode(null);
                scaleTransition.setNode(null);
                parallelTransition.stop();
                parallelTransition.setNode(null);
            });
        });
    }

    /**
     * create a new Label
     * @param text - text of label
     * @return - new Label
     */
    private Label getNewLabel(String text){
        Label label = new Label(text);
        label.setPrefWidth(Constants.LABEL_WIDTH_PREF);
        label.setPrefHeight(Constants.LABEL_HEIGHT_PREF);
        return label;
    }

    private void setUsersMapPosition(){
        try {
            List<User> users = userDao.getAll();
            users.stream()
                    .forEach((user) -> usersMapPosition.put(user.getPersonal_identifier(), user.getPosition()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * create a new Combobox<String> with specified params
     * @return - new Combobox
     */
    protected ComboBox<String> getComboboxControl(){
        ComboBox<String> attributeComboBox = new ComboBox<>();
        attributeComboBox.setMinWidth(Constants.COMBOBOX_WIDTH_MIN);
        attributeComboBox.setPrefWidth(Constants.COMBOBOX_WIDTH_PREF);
        attributeComboBox.setMaxWidth(Constants.COMBOBOX_WIDTH_MAX);
        attributeComboBox.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        return attributeComboBox;
    }

    /**
     * create a new TextField with specified params
     * @param attribute - prompt value
     * @return - new TextField
     */
    protected TextField getTextFieldControl(String attribute){
        TextField textField = new TextField();
        textField.setPromptText(attribute);
        textField.setPrefWidth(Constants.TEXTFIELD_WIDTH_PREF);
        textField.setPrefHeight(Constants.TEXTFIELD_HEIGHT_PREF);
        return textField;
    }

    /**
     * create a new combobox control
     * with default user position values
     * @return
     */
    protected ComboBox<String> getDefaultUserRoleComboboxControl(String ...isSpecifiedRole){
        ComboBox<String> positionCombobox = getComboboxControl();
        positionCombobox.getItems().addAll(((isSpecifiedRole == null) ? Constants.USERS_ROLES : isSpecifiedRole));
        return positionCombobox;
    }

    /**
     * check if all fields are not empty or null
     * @return
     */
    protected boolean checkAllFieldsCompletion() {
        return attributesControls.stream().map((attributeControl) -> {
            String value = null;
            if (attributeControl instanceof ComboBox) {
                value = ((ComboBox<String>) attributeControl).getValue();
            } else {
                value = ((TextField) attributeControl).getText();
            }
            return value;
        }).noneMatch((value) -> (value == null || value.isEmpty()));
    }

    /**
     * get all values is fields in a list
     * @return list with values
     */
    protected  ArrayList<String> getFieldsValues() {
        ArrayList<String> values = new ArrayList<>();
        attributesControls.stream().forEach((attributeControl) -> {
                    if (attributeControl instanceof TextField) {
                        values.add(((TextField) attributeControl).getText());
                    } else {
                        values.add(((ComboBox<String>) attributeControl).getValue());
                    }
                });
        return values;
    }

    /**
     *  update table
     *  clear all fields
     *  set the selected value in table view of specified selection
     *  and scroll to selection
     * @param selection selected value in table view
     */
    protected void updateView(int selection){
        populateView(null);          //update tableView
        clearButtonHandler();    //clear all fields
        //set selection on new value
        tableView.getSelectionModel().select(selection);
        tableView.scrollTo(selection);
    }

    /**
     * set ID ReadOnly
     * @param readOnly false -is readOnly, true is editable
     */
    protected void setIdReadOnly(boolean readOnly){
        if (attributesControls.get(0) instanceof ComboBox){
            ((ComboBox<String>) attributesControls.get(0)).setDisable(readOnly);
        } else {
            ((TextField) attributesControls.get(0)).setEditable(readOnly);
        }
    }

    /**
     * clear all fields
     */
    @FXML public  void clearButtonHandler(){
        attributesControls.stream().forEach((attributeControl) -> {
            if (attributeControl instanceof ComboBox) {
                ((ComboBox<String>) attributeControl).setValue("");
            } else {
                ((TextField) attributeControl).setText("");
            }
        });
    }

    /**
     * populate table view, with values if is not null
     * @param values - value list
     */
    protected abstract void populateView(List<T> values);

    /**
     * get TextField or Combobox id dependency
     * if value is forgein key
     * @param attribute - name atribute
     * @return - a new control
     */
    protected abstract Control getControlHandler(String attribute);
    /**
     * insert button handler
     */
    @FXML public abstract void insertButtonHandler();
    /**
     * update button handler
     */
    @FXML public abstract void updateButtonHandler();

    /**
     * delete button handler
     */
    @FXML public abstract void deleteButtonHandler();
    /**
     * search button handler
     */
    @FXML public abstract void searchButtonHandler();

    /**
     * checkbox handler
     */
    @FXML public abstract void infChecckBoxHandler();
}
