package com.srg.controller.admin.tableviewcontroller;

import com.srg.controller.DialogController;
import com.srg.dao.AbilityDao;
import com.srg.dao.BasicCRUDDao;
import com.srg.dao.DaoFactory;
import com.srg.model.Ability;
import com.srg.model.SpecialityDoctor;
import com.srg.util.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by administrator on 7/29/15.
 */
public class TableViewAbilityController extends TableViewAbstractController<Ability> {

    private BasicCRUDDao<SpecialityDoctor> daoSpecialityDoctor;
    private BasicCRUDDao<Ability> abilityDao;

    private Ability ability;

    public TableViewAbilityController() {
        super(Constants.TABLE_ABILITY);
        daoSpecialityDoctor = DaoFactory.getDao(Constants.TABLE_SPECIALITY_DOCTOR);
        abilityDao = DaoFactory.getDao(Constants.TABLE_ABILITY);
    }

    @FXML
    public void initialize(){
        super.initialize();
        //set PK readOnly
        setIdReadOnly(false);
        infChecckBox.setDisable(true);

        tableView.getSelectionModel().selectedItemProperty().addListener(event -> {
            ability = tableView.getSelectionModel().getSelectedItem();
            if (ability != null) {
                ((TextField) attributesControls.get(0)).setText(ability.getId());
                ((TextField) attributesControls.get(1)).setText(ability.getName());

                try {
                    SpecialityDoctor specialityDoctor = daoSpecialityDoctor.getById(ability.getId_speciality_doctor());
                    ((ComboBox<String>)attributesControls.get(2)).setValue(specialityDoctor.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void populateView(List<Ability> abilityList) {
        ObservableList<Ability> abilities = FXCollections.observableArrayList();

        if (abilityList == null || abilityList.isEmpty()) {
            try {
                abilities.addAll(abilityDao.getAll());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            abilities.addAll(abilityList);
        }
        tableView.setItems(abilities);
    }

    @Override
    protected Control getControlHandler(String attribute) {

        if (attribute.equals("id_speciality_doctor")){
            ComboBox<String> attributeComboBox = getComboboxControl();
            try {
                List<SpecialityDoctor> specialityDoctors = daoSpecialityDoctor.getAll();
                specialityDoctors.stream().forEach(
                        (specialityDoctor) -> attributeComboBox.getItems().add(specialityDoctor.toString()));

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return attributeComboBox;
        }

        return getTextFieldControl(attribute);
    }

    @Override
    public void insertButtonHandler() {
        //checkDuplicateAbilityOfSpecialityDoctor()
        if (!checkAllFieldsCompletion()){
            //System.out.println("ERROR (insertButtonHandler (ability)) -> Complete all fields !");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_COMPLETE_ALL_FIELDS);
            dialogController.start();
            return;
        }

        Ability newAbility = new Ability(getAbilityFieldsValuesList());

        if (checkDuplicateAbilityOfSpecialityDoctor(newAbility)){
            //System.out.println("Ability exist in list of doctorspeciality abilities ");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_DUPLICATE_VALUE);
            dialogController.start();
            return;
        }

        if (Constants.DEBUG){
            System.out.println("Ability inserted");
        }

        try {
            abilityDao.add(newAbility);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //update tableview
        updateView(tableView.getItems().size() - 1);
        clearButtonHandler();
    }

    @Override
    public void updateButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if ((tableViewSelectedIndex == -1) || (!checkAllFieldsCompletion())){
            //System.out.println("ERROR (updateButtonHandler (ability)) -> Are empty fields or wasn't select a value for update");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE_OR_ARE_EMPTY_FIELDS);
            dialogController.start();
            return;
        }

        Ability updateAbility = new Ability(getAbilityFieldsValuesList());

        if (Constants.DEBUG){
            System.out.println("Ability update");
        }

        try {
            abilityDao.update(updateAbility);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //update tableview
        updateView(tableView.getItems().size() - 1);
        clearButtonHandler();
    }

    @Override
    public void deleteButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if (tableViewSelectedIndex == -1){
            //System.out.println("ERROR (deleteButtonHandler (ability)) -> Select an Ability ");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE);
            dialogController.start();
            return;
        }

        if (Constants.DEBUG){
            System.out.println("Ability deleted");
        }

        try {
            abilityDao.delete(ability.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //update view
        updateView(0);
        clearButtonHandler();
    }

    @Override
    public void searchButtonHandler() {
        ArrayList<String> values = getAbilityFieldsValuesList();

        if (values == null || values.isEmpty()){
            populateView(null);

        } else {
            try {
                List<Ability> abilities =
                        ((AbilityDao)abilityDao)
                                .search(new Ability(values));

                populateView(abilities);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void infChecckBoxHandler() {

    }

    @Override
    public void clearButtonHandler() {
        super.clearButtonHandler();
    }

    private ArrayList<String> getAbilityFieldsValuesList(){
        ArrayList<String> values = getFieldsValues();

        if (values == null || values.isEmpty()){
            return null;
        }

        if ((values.get(2) != null) && values.get(2).contains("/")) {
            String cnp = new StringTokenizer(values.get(2), "/").nextToken().trim();
            values.set(2, cnp);
        }
        return values;
    }

    private boolean checkDuplicateAbilityOfSpecialityDoctor(Ability ability){
        try {
            ArrayList<String> abilitiesList = ((AbilityDao)abilityDao).getAbilitiesBySpecialityDoctorID(ability.getId_speciality_doctor());
            System.out.println(abilitiesList);
            System.out.println(abilitiesList == null);
            if (abilitiesList != null && !abilitiesList.isEmpty()){
                return abilitiesList.contains(ability.getName());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    protected boolean checkAllFieldsCompletion() {
        int k = 0;
        for (Control control : attributesControls){
            k++;
            String value = null;
            if (control instanceof ComboBox) {
                value = ((ComboBox<String>) control).getValue();
            } else {

                value = ((k == 1) ? "checked" : ((TextField) control).getText());
            }

            if (value == null || value.isEmpty()){
                return false;
            }
        }
        return true;
    }
}
