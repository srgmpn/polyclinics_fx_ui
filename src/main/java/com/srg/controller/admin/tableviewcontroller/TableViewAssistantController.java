package com.srg.controller.admin.tableviewcontroller;

import com.srg.controller.DialogController;
import com.srg.dao.AssitantDao;
import com.srg.dao.BasicCRUDDao;
import com.srg.dao.DaoFactory;
import com.srg.model.Assistant;
import com.srg.util.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/29/15.
 */
public class TableViewAssistantController extends TableViewAbstractUsersController<Assistant> {

    private BasicCRUDDao<Assistant> assistantDao;
    private Assistant assistant;

    public TableViewAssistantController() {
        super(Constants.TABLE_ASSITANT);
        assistantDao = DaoFactory.getDao(Constants.TABLE_ASSITANT);
    }

    @FXML
    public void initialize(){
        super.initialize();

        tableView.getSelectionModel().selectedItemProperty().addListener(event -> {

            assistant = tableView.getSelectionModel().getSelectedItem();
            if (assistant != null){
                //set PK readOnly
                setIdReadOnly((infChecckBox.isSelected() ? false : true));

                fillFieldsOnSelectionIsTableViewCell();
            }
        });
    }

    @Override
    protected void populateView(List<Assistant> assistantList) {
        ObservableList<Assistant> assistants = FXCollections.observableArrayList();

        if (assistantList == null || assistantList.isEmpty()) {

            try {
                assistants.addAll(assistantDao.getAll());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            assistants.addAll(assistantList);
        }
        tableView.setItems(assistants);
    }

    @Override
    protected Control getControlHandler(String attribute) {
        return super.getControlHandlerUser(attribute, Constants.ROLE4);
    }

    @Override
    public void insertButtonHandler() {
        if (!checkAllFieldsCompletion()){
            //System.out.println("ERROR (insertButtonHandler (assitant)) -> Complete all fields !");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_COMPLETE_ALL_FIELDS);
            dialogController.start();
            return;
        }
        if (!checkValidPersonalIdentifier(getAssistantFieldsValuesList().get(0))){
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_LENGHT_OF_PERSONAL_IDENTIFIER_ISNOT_13);
            dialogController.start();
            return;
        }
        Assistant newAssistant = new Assistant(getAssistantFieldsValuesList());
        try {
            if (checkDulicateValue(newAssistant.getPersonal_identifier())) {
                //System.out.println("ERROR (insertButtonHandler (assistant)) -> Duplicate Value \"Personal Identifier\"");
                DialogController dialogController = new DialogController();
                dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_DUPLICATE_VALUE);
                dialogController.start();
                return;
            }

            if (Constants.DEBUG){
                System.out.println("Assitant inserted");
            }
            if (infChecckBox.isSelected()){
                super.userAndEmployeeDaoInsert(4, 3);
            }
            //add new Asstant
            assistantDao.add(newAssistant);
            super.updateAndClearView(tableView.getItems().size() - 1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if ((tableViewSelectedIndex == -1) || (!checkAllFieldsCompletion())){
            //System.out.println("ERROR (updateButtonHandler (assitant)) -> Are empty fields or was not select a value for update");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE_OR_ARE_EMPTY_FIELDS);
            dialogController.start();
            return;
        }
        Assistant updateAssistant = new Assistant(getAssistantFieldsValuesList());
        try {
            if (Constants.DEBUG){
                System.out.println("Assitant updated");
            }
            if (infChecckBox.isSelected()){
                super.userAndEmployeeDaoUpdate(4, 3);
            }
            //add new Asstant
            assistantDao.update(updateAssistant);
            //update tableview
            super.updateAndClearView(tableViewSelectedIndex);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteButtonHandler() {
        super.deleteButtonHandleUser(assistant.getPersonal_identifier());
    }

    @Override
    public void searchButtonHandler() {
        ArrayList<String> values = getAssistantFieldsValuesList();

        try {
            if (values == null || values.isEmpty()){
                populateView(null);

            } else {
                List<Assistant> assistants = ((AssitantDao)assistantDao).search(new Assistant(values));
                populateView(assistants);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void infChecckBoxHandler() {
        super.infChecckBoxHandlerUser(Constants.TABLE_USER, Constants.TABLE_EMPLOYEE);
    }

    @Override
    public void clearButtonHandler() {
        super.clearButtonHandleUser();
    }

    private void fillFieldsOnSelectionIsTableViewCell(){
        try {
            employee = daoEmployee.getById(assistant.getPersonal_identifier());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (!infChecckBox.isSelected()) {
            ((ComboBox<String>) attributesControls.get(0)).setValue(employee.toString());
            setAssitantFieldsValues(1, 2);
            return;
        }

        try {
            user = userDao.getById(assistant.getPersonal_identifier());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //set values in controls
        super.setUserFieldsValues(user, 4);
        super.setEmployeeFieldsValues(employee, attributesControls.size()-4, attributesControls.size()-3);
        setAssitantFieldsValues(attributesControls.size()-2, attributesControls.size()-1);
    }

    private ArrayList<String> getAssistantFieldsValuesList(){
        if (infChecckBox.isSelected()){
            int size = super.getFieldsValues().size();
            return super.getPersonalValuesListIFISCheckFullInformation(0, size-2, size-1);
        }
        return super.getPersonalValuesList();
    }

    private void setAssitantFieldsValues(int index1, int index2){
        ((TextField) attributesControls.get(index1)).setText(assistant.getSpeciality());
        ((TextField) attributesControls.get(index2)).setText(assistant.getDegree());
    }

    /**
     * check if exist in assistant table assistant with
     * specified personal identifier
     * @param cnp
     * @return
     * @throws SQLException
     */
    private boolean checkDulicateValue(String cnp) throws SQLException {
        return ((assistantDao.getById(cnp) != null) ? true : false);
    }

}
