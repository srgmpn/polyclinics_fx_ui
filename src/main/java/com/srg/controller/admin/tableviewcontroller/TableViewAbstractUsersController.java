package com.srg.controller.admin.tableviewcontroller;

import com.srg.controller.DialogController;
import com.srg.dao.BasicCRUDDao;
import com.srg.dao.DaoFactory;
import com.srg.model.Employee;
import com.srg.model.User;
import com.srg.util.Constants;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by administrator on 8/4/15.
 */
public abstract class TableViewAbstractUsersController<T> extends TableViewAbstractController<T> {

    protected BasicCRUDDao<Employee> daoEmployee;
    protected User user;
    protected Employee employee;

    protected TableViewAbstractUsersController(String tableName) {
        super(tableName);
        daoEmployee = DaoFactory.getDao(Constants.TABLE_EMPLOYEE);
    }

    /**
     * get new Control (Combobox or TextField) for all
     * Controllers which work with users
     * @param attribute - atribute
     * @param userRole - user
     * @return - new Control
     */
    protected Control getControlHandlerUser(String attribute, String userRole){
        if (attribute.equals("personal_identifier") && !infChecckBox.isSelected()){
            ComboBox<String> attributeComboBox = super.getComboboxControl();
            try {
                List<Employee> employees = daoEmployee.getAll();
                employees.stream().forEach(
                        (employeeL) -> {
                            //load only assitant employee
                            if (usersMapPosition.get(employeeL.getPersonal_identifier()).equals(userRole)) {
                                attributeComboBox.getItems().add(employeeL.toString());
                            }
                        });

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return attributeComboBox;
        }

        if (attribute.equals("position")){
            return super.getDefaultUserRoleComboboxControl(userRole);
        }
        return getTextFieldControl(attribute);
    }

    /**
     * set user values in fields when is selected a row is tableview
     * if is select full information checkbox is present and employee fields and ...
     * then k represent user's fields number
     * @param k - nr excepted attribute controls
     */
    protected void setUserFieldsValues(User user, int k){
        for (int i = 0; i < attributesControls.size() - k; ++i){
            if (attributesControls.get(i) instanceof ComboBox){
                ((ComboBox<String>)attributesControls.get(i)).setValue(user.getValues().get(i));
            } else {
                ((TextField) attributesControls.get(i)).setText(user.getValues().get(i));
            }
        }
    }

    /**
     * set employee fields values
     * @param index1 - salary field index in attributes controls list
     * @param index2 - working hours field index in attributes controls list
     */
    protected void setEmployeeFieldsValues(Employee employee, int index1, int index2){
        ((TextField) attributesControls.get(index1)).setText(employee.getSalary());
        ((TextField) attributesControls.get(index2)).setText(employee.getWorking_hours());
    }

    /**
     *
     * @param k
     * @return
     */
    protected ArrayList<String> getUserFieldsValuesList(int k){
        ArrayList<String> values = super.getFieldsValues();
        while (k > 0) {
            values.remove(values.size() - 1);
            --k;
        }
        return values;
    }

    /**
     * get Values is fields in an arrayList
     * @param index1 - salary field index in attributes controls list
     * @param index2 - working hours field index in attributes controls list
     * @return - list with values
     */

    protected ArrayList<String> getEmployeeFieldsValuesList(int index1, int index2){
        ArrayList<String> values = super.getFieldsValues();
        ArrayList<String> employeeValues = new ArrayList<>(3);
        employeeValues.add(values.get(0));
        employeeValues.add(values.get(values.size()-index1));
        employeeValues.add(values.get(values.size()-index2));
        return employeeValues;
    }

    /**
     * if is not selected full information mode
     * get only personal user values, get CNP is combobox value
     * @return - list with values
     */
    protected ArrayList<String> getPersonalValuesList(){
        ArrayList<String> values = super.getFieldsValues();

        if (values == null || values.isEmpty()){
            return null;
        }

        if ((values.get(0) != null) && values.get(0).contains("/")) {
            String cnp = new StringTokenizer(values.get(0), "/").nextToken().trim();
            values.set(0, cnp);
        }
        return values;
    }

    /**
     *
     * @param index
     * @return
     */
    protected ArrayList<String> getPersonalValuesListIFISCheckFullInformation(int ...index){
        ArrayList<String> values = super.getFieldsValues();
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i : index){
            arrayList.add(values.get(i));
        }
        return arrayList;
    }

    /**
     * invoke clearButtonHandle of super class and set editable
     * primary key
     */
    protected void clearButtonHandleUser(){
        super.clearButtonHandler();
        //set PK writeable
        super.setIdReadOnly((infChecckBox.isSelected() ? true : false));
    }

    /**
     *
     * @param presonalIdentifier
     */
    protected void deleteButtonHandleUser(String presonalIdentifier){
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if (tableViewSelectedIndex == -1){
            //System.out.println("ERROR (deleteButtonHandler ) -> Select an value");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE);
            dialogController.start();
            return;
        }

        try {
            if (Constants.DEBUG){
                System.out.println("User deleted");
            }
            //assitant : employee : user is in relation with  1:1:1 and
            //if delete a assitant it means delete an user in cascade
            userDao.delete(presonalIdentifier);
            //update tableview
            updateAndClearView(0);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param parentsTablesName
     */
    protected void infChecckBoxHandlerUser(String ...parentsTablesName){
        if (infChecckBox.isSelected()){
            super.setDefaultFieldsTable(true, parentsTablesName);
        } else {
            super.setDefaultFieldsTable(false, null);
        }
    }

    /**
     *
     * @param index1
     * @param index2
     */
    protected void userAndEmployeeDaoInsert(int index1, int index2){
        try {
            userDao.add(new User(getUserFieldsValuesList(index1)));
            daoEmployee.add(new Employee(getEmployeeFieldsValuesList(index1, index2)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     *
     * @param index1
     * @param index2
     */
    protected void userAndEmployeeDaoUpdate(int index1, int index2){
        try {
            userDao.update(new User(getUserFieldsValuesList(index1)));
            daoEmployee.update(new Employee(getEmployeeFieldsValuesList(index1, index2)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param index
     */
    protected void updateAndClearView(int index){
        updateView(index);
        clearButtonHandleUser();
    }

    protected boolean checkValidPersonalIdentifier(String personalIdentifierLength){
        return (personalIdentifierLength.length() == 13);
    }
}
