package com.srg.controller.admin.tableviewcontroller;

import com.srg.controller.DialogController;
import com.srg.core.RBACHandle;
import com.srg.dao.UserDao;
import com.srg.model.User;
import com.srg.util.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 7/29/15.
 */
public class TableViewUserController extends TableViewAbstractController<User> {

    private User user;

    public TableViewUserController() {
        super(Constants.TABLE_USER);
    }

    @FXML public void initialize(){
        super.initialize();
        infChecckBox.setDisable(true);

        tableView.getSelectionModel().selectedItemProperty().addListener(event -> {
            user = tableView.getSelectionModel().getSelectedItem();
            if (user != null) {
                //set rigth of selected user
                setEditableControls((getAccesRigth(user) ? false : true));
                //set cnp read only
                setIdReadOnly(false);
                //set values in controls
                for (int i = 0; i < attributesControls.size(); ++i){
                    if (attributesControls.get(i) instanceof ComboBox){
                        ((ComboBox<String>)attributesControls.get(i)).setValue(user.getValues().get(i));
                    } else {
                        ((TextField) attributesControls.get(i)).setText(user.getValues().get(i));
                    }
                }
            }
        });
    }

    @Override
    protected void populateView(List<User> usersList) {

        if (Constants.DEBUG){
            System.out.println("Populate user view");
        }

        ObservableList<User> users = FXCollections.observableArrayList();

        if (usersList == null || usersList.isEmpty()) {
            try {
                users.addAll(userDao.getAll());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            users.addAll(usersList);
        }
        tableView.setItems(users);
    }

    @Override
    protected Control getControlHandler(String attribute) {

        if (attribute.equals("position")){
            return getDefaultUserRoleComboboxControl(Constants.USERS_ROLES);
        }
        return getTextFieldControl(attribute);
    }

    @Override
    public void insertButtonHandler() {

        //check if fields are not empty
        if (!checkAllFieldsCompletion()) {
            //System.out.println("ERROR (insertButtonHandler (user)) -> Complete all fields !");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_COMPLETE_ALL_FIELDS);
            dialogController.start();
            return;
        }

        //new user
        User newUser = new User(getFieldsValues());
        if (!checkValidPersonalIdentifier(newUser)){
            //System.out.println("ERROR (insertButtonHandler (user)) -> Personal Identifier (length = 13 ?)");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_LENGHT_OF_PERSONAL_IDENTIFIER_ISNOT_13);
            dialogController.start();
            return;
        }
        try {
            //chech is value duplicat
            if (checkDulicateValue(newUser.getPersonal_identifier())) {
                //System.out.println("ERROR (insertButtonHandler (user)) -> Duplicate Value \"Personal Identifier\"");
                DialogController dialogController = new DialogController();
                dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_DUPLICATE_VALUE);
                dialogController.start();
                return;
            }

            if (Constants.DEBUG){
                System.out.println("User inserted");
            }
               //insert new user
           // int result = userDao.add(newUser);
               //update view
            updateView(tableView.getItems().size() - 1);
            clearButtonHandler();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if ((tableViewSelectedIndex == -1) || (!checkAllFieldsCompletion())){
            //System.out.println("ERROR (updateButtonHandler (user)) -> Are empty fields or was not select a value for update");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE_OR_ARE_EMPTY_FIELDS);
            dialogController.start();
            return;
        }
        if (getAccesRigth(user)){
            //System.out.println("ERROR (updateButtonHandler (user)) -> Don't allowed to update");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_ACCESS_NOT_ALLOWED_DO_OPERTAION);
            dialogController.start();
            return;
        }
        //new user
        User updateUser = new User(getFieldsValues());
        try {
            if (Constants.DEBUG){
                System.out.println("User updated");
            }
            //insert new user
            userDao.update(updateUser);
            //update view
            updateView(tableViewSelectedIndex);
            clearButtonHandler();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteButtonHandler() {
        int tableViewSelectedIndex = tableView.getSelectionModel().getSelectedIndex();

        if (tableViewSelectedIndex == -1){
            //System.out.println("ERROR (deleteButtonHandler (user)) -> Select an user ");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_A_VALUE);
            dialogController.start();
            return;
        }

        if (getAccesRigth(user)){
            //System.out.println("ERROR (deleteButtonHandler (user)) -> Don't allowed to delete");
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_ACCESS_NOT_ALLOWED_DO_OPERTAION);
            dialogController.start();
            return;
        }

        try {
            if (Constants.DEBUG){
                System.out.println("User deleted");
            }
            userDao.delete(user.getPersonal_identifier());
            //update View
            updateView(0);
            clearButtonHandler();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void searchButtonHandler() {
        try {
            ArrayList<String> values = getFieldsValues();

            if (values.isEmpty() && values.get(9) == null) {
                populateView(null);
            } else {
                List<User> users = ((UserDao) userDao).search(new User(values));
                populateView(users);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void infChecckBoxHandler() {
    }

    @Override
    public void clearButtonHandler(){
        super.clearButtonHandler();
        //set editable
        setEditableControls(true);
    }

    /**
     * verif if selected user is admin or super-admin
     * and curent user has right for edit admin role
     * @param user
     * @return true - if don't has right, else false
     */
    private boolean getAccesRigth(User user){
        return (RBACHandle.getInstance().getUser().getPosition().equals(user.getPosition())
                || user.getPosition().contains(RBACHandle.getInstance().getUser().getPosition()));
    }

    /**
     * set true or false
     * if user has right for edit selected user
     * @param editable
     */
    private void setEditableControls(boolean editable){
        attributesControls.stream().forEach((attributeControl) ->{
            if (attributeControl instanceof TextField){
                ((TextField)attributeControl).setEditable(editable);
            } else {
                ((ComboBox<String>)attributeControl).setDisable(!editable);
            }
        });
    }

    /**
     * check if exist in user table user with
     * specified personal identifier
     * @param cnp
     * @return
     * @throws SQLException
     */
    private boolean checkDulicateValue(String cnp) throws SQLException {
        return ((userDao.getById(cnp) != null) ? true : false);
    }

    /**
     * chech if length of field personal identifier = 13
     * @return
     */
    private boolean checkValidPersonalIdentifier(User newUser){
        return (user.getPersonal_identifier().length() == 13);
    }
}
