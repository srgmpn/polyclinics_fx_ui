package com.srg.controller;

import com.srg.dao.BasicCRUDDao;
import com.srg.dao.DaoFactory;
import com.srg.dao.ScheduleDao;
import com.srg.model.HumanResource;
import com.srg.model.Schedule;
import com.srg.model.User;
import com.srg.util.Constants;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.StringConverter;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 8/3/15.
 */
public class HumanResourceController extends AbstractController{

    private BasicCRUDDao<Schedule> scheduleDao;

    private ArrayList<User> users;

    private HumanResource humanResource;
    private ArrayList<Control> attibutesControls;

    @FXML private Button insertButton;
    @FXML private Button updateButton;
    @FXML private Button deleteButton;
    @FXML private Button searchButton;
    @FXML private Button clearButton;

    @FXML private ToggleGroup scheduleToogleGroup;
    @FXML private RadioButton vacationRB;
    @FXML private RadioButton scheduleRB;

    @FXML private VBox vbContainer;

    @FXML private Label fnameLabel;
    @FXML private Label lnameLabel;
    @FXML private Label positionLabel;

    @FXML private TextField fnameTextField;
    @FXML private TextField lnameTextField;
    @FXML private ComboBox<String> positionCombobox;

    @FXML private TableView<HumanResource> tableView;

    public HumanResourceController() {
        super(Constants.Human_Resource_Mangament, false);

        scheduleDao = DaoFactory.getDao(Constants.TABLE_SCHEDULE);
        //load users from db
        loadUserOfDB();
    }

    @FXML
    public void initialize(){
        super.initialize();

        setButtonsEfects();

        //set search button effects
        searchButton.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> super.setButtonMouseEntredEfects(searchButton));
        searchButton.addEventHandler(MouseEvent.MOUSE_EXITED, event-> super.setButtonMouseExitEfects(searchButton));

        setTableColumnsNames();

        //set table view selection listner
        tableView.getSelectionModel().selectedItemProperty()
                .addListener(event -> handleTableViewSelection(tableView.getSelectionModel().getSelectedItem()));

        //set toogle radiobuttons listener
        scheduleToogleGroup.selectedToggleProperty().addListener(
                (ObservableValue<? extends Toggle> observableValue, Toggle oldToggle, Toggle newToggle) -> {
                    if (scheduleToogleGroup.getSelectedToggle() != null) {
                        //update view
                        populateView(null);
                        //clear all generated textfields
                        vbContainer.getChildren().clear();
                        //set userlabes empty
                        setUserLabelsValues("", "", "");
                    }
                }
        );

        //populate combobox with default values
        positionCombobox.getItems().addAll(Constants.ROLE3, Constants.ROLE4, Constants.ROLE5, Constants.ROLE6, Constants.ROLE7);
    }

    public void start(){
        //load the controller using reflection
        try {
            scene = new Scene((Parent) FXMLLoader.load(getClass().getResource(Constants.FXML_HUMANRESOURCE)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.start();
    }

    @Override
    protected void setOperationMenu() {

    }

    @Override
    protected EventHandler<Event> getMouseEventHandler() {
        return null;
    }

    @FXML
    private void insertButtonHandler(ActionEvent event){
        if (!checkScheduleFieldsValues()){
            return;
        }
       try {
            scheduleDao.add(getSchedule());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        updateView();
    }

    @FXML
    private void updateButtonHandler(ActionEvent event){
        if (!checkScheduleFieldsValues()){
            return;
        }
        try {
            scheduleDao.update(getSchedule());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        updateView();
    }

    @FXML
    private void deleteButtonHandler(ActionEvent event){
        if (!checkScheduleFieldsValues()){
            return;
        }
        try {
            scheduleDao.delete(humanResource.schedule().getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        updateView();
    }

    @FXML
    private void searchButtonHandler(ActionEvent event){

        if (fnameTextField.getText().isEmpty() && lnameTextField.getText().isEmpty() && (positionCombobox.getValue() == null)){
            populateView(null);
            return;
        }
        ArrayList<HumanResource> humanResources = new ArrayList<>();
        users.stream().forEach(
                (user) ->{
                    if (isValidSearchCondition(user)) {
                        handleUserSchedules(user, humanResources);
                    }
                });
        populateView(humanResources);
        positionCombobox.setValue(null);
    }

    @FXML
    private void clearButtonHandler(ActionEvent event) {
        clearAttributesTextFields();
    }

    /**
     * load all users from db in the list users
     */
    private void loadUserOfDB(){
        //load all users without administrators and super-administrators
        BasicCRUDDao<User> userDao  = DaoFactory.getDao(Constants.TABLE_USER);
        try {
            users = new ArrayList<>();
            userDao.getAll().stream().forEach((user) -> {
                if (!user.getPosition().equals(Constants.ROLE1) && !user.getPosition().equals(Constants.ROLE2)){
                    users.add(user);
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * create a new Schedule object
     * get Dates form fields
     * @return
     */
    private Schedule getSchedule(){
        Schedule schedule = new Schedule();
        schedule.setId(humanResource.schedule().getId());
        schedule.setTime_slot(((TextField)attibutesControls.get(0)).getText());
        schedule.setWeekday(((ComboBox<String>) attibutesControls.get(1)).getValue());

        String schedukeDate = ((((DatePicker)attibutesControls.get(2)).getValue() != null )?
                getLocalDateFormat(((DatePicker) attibutesControls.get(2)).getValue().toString()) :
                null);

        schedule.setSchedule_date(schedukeDate);
        schedule.setLocation(((TextField)attibutesControls.get(3)).getText());
        schedule.setId_employee(humanResource.user().getPersonal_identifier());
        return schedule;
    }

    /**
     * update table view with new values
     * clear attributes text fields
     * clear user labels
     */
    private void updateView(){
        populateView(null);
        clearAttributesTextFields();
        setUserLabelsValues("", "", "");
    }

    /**
     * clear attributes text fields
     */
    private void clearAttributesTextFields(){
        if (attibutesControls == null || attibutesControls.isEmpty()) {
            return;
        }
        attibutesControls.stream().forEach((control) -> {
            if (control instanceof TextField) {
                ((TextField) control).setText("");
            } else if (control instanceof ComboBox) {
                ((ComboBox) control).setValue("");
            } else {
                ((DatePicker) control).setValue(null);
            }
        });
    }

    /**
     * verif if exit curent user is equals with specified values in search fields
     * @param user - curent selected user
     * @return - if is equal return true, else false
     */
    private boolean isValidSearchCondition(User user){
        return getFieldSearchValue(fnameTextField.getText(), user.getFirst_name()).equalsIgnoreCase(user.getFirst_name())
                && getFieldSearchValue(lnameTextField.getText(), user.getLast_name()).equalsIgnoreCase(user.getLast_name())
                && getFieldSearchValue(positionCombobox.getValue(), user.getPosition()).equalsIgnoreCase(user.getPosition());
    }

    /**
     * verif if fieldValue is null or empty return defaultvalue
     * else fieldValue
     * @param fieldValue
     * @param defaultValue
     * @return
     */
    private String getFieldSearchValue(String fieldValue, String defaultValue){
        return ((fieldValue == null || fieldValue.isEmpty()) ? defaultValue : fieldValue);
    }

    /**
     * set User Labels values
     * @param fname
     * @param lname
     * @param position
     */
    private void setUserLabelsValues(String fname, String lname, String position){
        fnameLabel.setText(fname);
        lnameLabel.setText(lname);
        positionLabel.setText(position);
    }

    /**
     *create and add all attributes TextFields
     * clear all clidren controls is vbox contrainer and add new
     * and set new values
     */
    private void createAndSetScheduleTextFields(){
        attibutesControls = new ArrayList<>();
        vbContainer.getChildren().clear();
        vbContainer.getChildren().addAll(getHBoxContainer(49, getLabel("TimeSlot"), getTextField("TimeSlot", humanResource.getTimeSlot())), getHorizontalSeparator());
        vbContainer.getChildren().addAll(getHBoxContainer(47, getLabel("Weekday"), getComboboxControl(humanResource.getWeekday().toLowerCase(), Constants.DAY_OF_WEEK)), getHorizontalSeparator());
        vbContainer.getChildren().addAll(getHBoxContainer(10, getLabel("ScheduleDate"), getDatePicker(humanResource.getScheduleDate())), getHorizontalSeparator());
        vbContainer.getChildren().addAll(getHBoxContainer(52, getLabel("Location"), getTextField("Location", humanResource.getLocation())), getHorizontalSeparator());
    }

    /**
     *
     */
    private void createAndSetVacantionTextFields() {
        vbContainer.getChildren().clear();
    }

    /**
     *  create a hbox contrainer with 2 controls
     * @param spacing
     * @return - new hbox
     */
    private HBox getHBoxContainer(double spacing, Label label, Control control){
        HBox hBox = new HBox();
        hBox.setSpacing(spacing);
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setPrefHeight(26);
        hBox.getChildren().addAll(label, control);
        return hBox;
    }

    /**
     * create and get a new Label with name = nameAttribute
     * @param nameAttribute
     * @return
     */
    private Label getLabel(String nameAttribute){
        Label label = new Label(nameAttribute + ":");
        label.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        label.setPadding(new Insets(0, 0, 0, 40));
        return label;
    }

    /**
     * create a new TextField and set value
     * @param nameAttribute - value is Prompt Text
     * @param value - value which is set in text field
     * @return - new textField
     */
    private TextField getTextField(String nameAttribute, String value){
        TextField textField = new TextField();
        textField.setPromptText(nameAttribute);
        textField.setPrefWidth(150);
        textField.setFont(Font.font(14));
        textField.setText(value);

        attibutesControls.add(textField);

        return textField;
    }

    /**
     * create and get a new Combobox control
     * @param defaultValue - default value
     * @param values - list of values
     * @return - new Combobox
     */
    private ComboBox<String> getComboboxControl(String defaultValue, String ...values){
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.getItems().addAll(values);
        comboBox.setValue(defaultValue);
        attibutesControls.add(comboBox);
        return comboBox;
    }

    /**
     * create a DatePicker, set setDayCellFactory, all old dates
     * inaccesible, and set converter
     * @param date
     * @return
     */
    private DatePicker getDatePicker(String date){
        //Locale.setDefault(new Locale("ro", "RO"));
        //Locale.setDefault(LocalDate.parse("dd/MM/yyyy"));
        DatePicker datePicker = new DatePicker();

        //set disable dates before current date
        datePicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(LocalDate.now())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        });

        //set a format date
        datePicker.setConverter(new StringConverter<LocalDate>() {
            private DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern(Constants.PATTERN_FORMAT_LOCALDATE_1);

            @Override
            public String toString(LocalDate object) {
                return ((object == null) ? "" : dateTimeFormatter.format(object));
            }

            @Override
            public LocalDate fromString(String string) {
                return ((string == null || string.trim().isEmpty()) ? null : LocalDate.parse(string, dateTimeFormatter));
            }
        });

        if (date != null && !date.isEmpty()) {
            datePicker.setValue(LocalDate.parse(date, DateTimeFormatter.ofPattern(Constants.PATTERN_FORMAT_LOCALDATE_1)));
        }
        attibutesControls.add(datePicker);
        return datePicker;
    }

    /**
     * parse local date a format dd/MM/yyyy
     * and get a string with new format
     * @param date - old date formate value
     * @return - new date format value
     */
    private String getLocalDateFormat(String date){
        return ((date == null) ? null :
                LocalDate.parse(date,
                        DateTimeFormatter
                            .ofPattern(Constants.PATTERN_FORMAT_LOCALDATE_2))
                            .format(DateTimeFormatter
                                    .ofPattern(Constants.PATTERN_FORMAT_LOCALDATE_1)));
    }

    /**
     *
     * @return new Separator
     */
    private Separator getHorizontalSeparator(){
        Separator separator = new Separator(Orientation.HORIZONTAL);
        return separator;
    }

    /**
     *set columns table name
     */
    private void setTableColumnsNames(){
        /*Method[] methods = HumanResource.class.getDeclaredMethods();
        ArrayList<String> columnsNamesList = new ArrayList();
        for (Method method : methods){
            if (method.getName().startsWith("get")){
                columnsNamesList.add(method.getName().substring(3));
            }
        }
        System.out.println(columnsNamesList);
        */
        final  String [] tableColumnsNames = {"fname", "lname", "position", "timeSlot", "weekday", "scheduleDate", "location"};
        tableView.setItems(null);
        if (tableView.getColumns() != null){
            tableView.getColumns().clear();
        }
        tableView.setEditable(true);

        for (String columnName : tableColumnsNames){
            TableColumn<HumanResource, String> column = new TableColumn<>(columnName);
            column.setMinWidth(tableView.getPrefWidth() / tableColumnsNames.length);
            column.setCellValueFactory(new PropertyValueFactory<>(columnName));
            tableView.getColumns().add(column);
        }
        populateView(null);
    }

    /**
     * populate a table view fields, if humanResourceList is null or empty
     * get all schedules of DB
     * else put humanResourceList
     * @param humanResourceList
     */
    private void populateView(ArrayList<HumanResource> humanResourceList){
        ObservableList<HumanResource> humanResources = FXCollections.observableArrayList();
        if (humanResourceList == null || humanResourceList.isEmpty()){
            humanResources.addAll(getAllHumanResources());
        } else {
            humanResources.addAll(humanResourceList);
        }
        tableView.setItems(humanResources);
    }

    /**
     * create a HumanResourceList, with
     * @return
     */
    private ArrayList<HumanResource> getAllHumanResources(){
        ArrayList<HumanResource> humanResources = new ArrayList<>();
        users.stream().forEach(
                (user) ->{
                    handleUserSchedules(user, humanResources);
                });
        return humanResources;
    }

    /**
     *
     * @param user
     * @param humanResources
     */
    private void handleUserSchedules(User user, ArrayList<HumanResource> humanResources){
        try {

            List<Schedule> schedules = ((ScheduleDao)scheduleDao).getSchedulesByEmplyeeID(user.getPersonal_identifier());
            if (schedules != null || !schedules.isEmpty()) {
                schedules.stream().forEach((schedule) -> humanResources.add(new HumanResource(user, schedule)));
            } else {
                humanResources.add(new HumanResource(user, new Schedule()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param humanResource
     */
    private void handleTableViewSelection(HumanResource humanResource){
        if (!vacationRB.isSelected() && !scheduleRB.isSelected()){
            DialogController dialogController = new DialogController();
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_SELECT_SCHEDULE_OR_VACANTION_RADIOBUTTON);
            dialogController.start();
            //System.out.println("Select a schedule or vacantion radiobutton value");
            return;
        }
        this.humanResource = humanResource;
        if (humanResource != null){
            setUserLabelsValues(humanResource.getFname(), humanResource.getLname(), humanResource.getPosition());

            if (vacationRB.isSelected()){
                createAndSetVacantionTextFields();
            }
            else {
                createAndSetScheduleTextFields();
            }
        }
    }

    private boolean checkScheduleFieldsValues(){
        DialogController dialogController = new DialogController();
        if (attibutesControls == null || ((TextField)attibutesControls.get(0)).getText().isEmpty()){
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_ALL_FIELDS_OR_TIMESLOT_IS_EMPTY);
            dialogController.start();
            return false;
        }
        //System.out.println(((ComboBox) attibutesControls.get(1)).getValue().toString());
        if ((((ComboBox<String>)attibutesControls.get(1)).getValue().isEmpty() && ((DatePicker)attibutesControls.get(2)).getValue() == null) ||
                (!((ComboBox<String>)attibutesControls.get(1)).getValue().isEmpty() && ((DatePicker)attibutesControls.get(2)).getValue() != null)){
            //System.out.println("Orarul poate fi sau general sau specific");
            dialogController.setProperties(Constants.ERROR, Constants.ICON_FILE_NAME, Constants.ERROR_GENERIC_OR_SPECIFIC);
            dialogController.start();
            return false;
        }
        return true;
    }

    /**
     * buttons effects
     */
    private void setButtonsEfects(){
        //set buttons efects
        final FadeTransition fadeTransition = new FadeTransition(Duration.seconds(1));
        fadeTransition.setFromValue(1.0f);
        fadeTransition.setToValue(0.8f);
        fadeTransition.setCycleCount(Timeline.INDEFINITE);
        fadeTransition.setAutoReverse(true);
        final ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(1));
        scaleTransition.setByX(0.1f);
        scaleTransition.setByY(0.1f);
        scaleTransition.setCycleCount(Timeline.INDEFINITE);
        scaleTransition.setAutoReverse(true);
        final ParallelTransition parallelTransition = new ParallelTransition(fadeTransition, scaleTransition);

        ArrayList<Button> buttonList = new ArrayList<>();
        buttonList.add(insertButton);
        buttonList.add(updateButton);
        buttonList.add(deleteButton);
        buttonList.add(clearButton);

        buttonList.stream().map((currentButton) -> {
            currentButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent event) -> {
                currentButton.setEffect(new DropShadow());
                fadeTransition.setNode(currentButton);
                scaleTransition.setNode(currentButton);
                parallelTransition.play();
            });
            return currentButton;
        }).forEach((currentButton) -> {
            currentButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent event) -> {
                currentButton.setEffect(null);
                parallelTransition.jumpTo(Duration.ZERO);
                fadeTransition.setNode(null);
                scaleTransition.setNode(null);
                parallelTransition.stop();
                parallelTransition.setNode(null);
            });
        });
    }

}
