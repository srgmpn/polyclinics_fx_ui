package com.srg.controller;

import com.srg.core.RBACHandle;
import com.srg.util.Constants;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Created by administrator on 8/3/15.
 */
public abstract class AbstractController {

    protected Stage window;
    protected Scene scene;

    private FadeTransition fadeTransition;
    private ScaleTransition scaleTransition;
    private ParallelTransition parallelTransition;

    @FXML protected Button logOutButton;
    @FXML protected Menu operationMenu;
    @FXML protected Label userNameStatusLabel;

    private String windowTitle;
    private boolean startFullScreen;

    public AbstractController(String windowTitle, boolean startFullScreen){
        this.windowTitle = windowTitle;
        this.startFullScreen = startFullScreen;
    }

    protected void initialize(){
        //set  status
        userNameStatusLabel.setText(RBACHandle.getInstance().getUser().getFirst_name() + " " +
                RBACHandle.getInstance().getUser().getLast_name());
        //set menu operation values
        setOperationMenu();
        //set image to logout button
        logOutButton.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> setButtonMouseEntredEfects(logOutButton));
        logOutButton.addEventHandler(MouseEvent.MOUSE_EXITED, event-> setButtonMouseExitEfects(logOutButton));
    }

    protected void start(){
        window = new Stage();
        window.setTitle(windowTitle);
        window.setScene(scene);
        window.setMaximized(startFullScreen);
        window.show();
    }

    @FXML
    protected void logoutButtonHandler(ActionEvent event){

        if (Constants.DEBUG) {
            System.out.println("Starting login AdminController ...");
        }

        new LoginController().start();
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @FXML
    protected void closeMenuItemHandler(ActionEvent event){
        Platform.exit();
    }

    @FXML
    protected void aboutMenuItemHandler(ActionEvent event){

    }

    protected void setButtonMouseEntredEfects(Button button){
        fadeTransition = new FadeTransition(Duration.seconds(1));
        fadeTransition.setFromValue(1.0f);
        fadeTransition.setToValue(0.8f);
        fadeTransition.setCycleCount(Timeline.INDEFINITE);
        fadeTransition.setAutoReverse(true);
        scaleTransition = new ScaleTransition(Duration.seconds(1));
        scaleTransition.setByX(0.1f);
        scaleTransition.setByY(0.1f);
        scaleTransition.setCycleCount(Timeline.INDEFINITE);
        scaleTransition.setAutoReverse(true);
        parallelTransition = new ParallelTransition(fadeTransition, scaleTransition);

        button.setEffect(new DropShadow());
        fadeTransition.setNode(button);
        scaleTransition.setNode(button);
        parallelTransition.play();
    }

    protected void setButtonMouseExitEfects(Button button){
        button.setEffect(null);
        parallelTransition.jumpTo(Duration.ZERO);
        fadeTransition.setNode(null);
        scaleTransition.setNode(null);
        parallelTransition.stop();
        parallelTransition.setNode(null);
    }

    protected abstract void setOperationMenu();
    protected abstract EventHandler<Event> getMouseEventHandler();
}
