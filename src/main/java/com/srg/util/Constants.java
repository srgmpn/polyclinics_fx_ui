package com.srg.util;

/**
 * Created by administrator on 7/11/15.
 */
public class Constants {

    public static final String ADMIN_MANAGEMENT = "Admin polyclinics management";
    public static final String Human_Resource_Mangament = "Human Resource Mangament";

    public static final String LOGIN_NAME_DIALOG = "Polyclinics management";
    public static final String ERROR_USERNAME_PASSWORD = "Either username or password are incorect";
    private static final String ERROR_ACCESS_NOT_ALLOWED = "You don't have enough rights to ";
    public static final String ERROR_ACCESS_NOT_ALLOWED_ACCESS_APP = ERROR_ACCESS_NOT_ALLOWED + "access the application!";
    public static final String ERROR_ACCESS_NOT_ALLOWED_DO_OPERTAION = ERROR_ACCESS_NOT_ALLOWED + "operate with selected user";
    public static final String ERROR = "ERROR";
    public static final String ERROR_ALL_FIELDS_OR_TIMESLOT_IS_EMPTY = "ALL fields or timeslot field are empty";
    public static final String ERROR_GENERIC_OR_SPECIFIC = "Schedule should be generic or specific";
    public static final String ERROR_SELECT_SCHEDULE_OR_VACANTION_RADIOBUTTON = "Select a schedule or vacantion radiobutton value";
    public static final String ERROR_SELECT_A_VALUE = "Select a value";
    public static final String ERROR_COMPLETE_ALL_FIELDS = "Complete all fields !";
    public static final String ERROR_SELECT_A_VALUE_OR_ARE_EMPTY_FIELDS = "Are empty fields or was not select a value";
    public static final String ERROR_DUPLICATE_VALUE = "Duplicate Value";
    public static final String ERROR_LENGHT_OF_PERSONAL_IDENTIFIER_ISNOT_13 = "Pesonal Identifier lenght should be 13 !";


    public static final Boolean DEBUG = true;

    public static final String MESSAGE = "Eroare la executia operatiei in baza de date";

    public static final String PREFIX_TABLE_NAME = "u_";

    public static final String DATABASE_NAME = "polyclinics";
    public static final String DATABASE_CONNECTION = "jdbc:mysql://localhost/";
    public static final String DATABASE_USERNAME = "root";
    public static final String DATABASE_PASSWORD = "123";

    public static final String TABLE_NAME = "TABLE_NAME";
    public static final String COLUMN_NAME = "COLUMN_NAME";
    public static final String IS_AUTOINCREMENT = "IS_AUTOINCREMENT";

    public static final String QUERY_INSERT_INTO = "INSERT INTO ";
    public static final String QUERY_SELECT = "SELECT ";
    public static final String QUERY_FROM = " FROM ";
    public static final String QUERY_WHERE = " WHERE ";
    public static final String QUERY_GROUP_BY = " GROUP BY ";
    public static final String QUERY__ORDER_BY = " ORDER BY ";
    public static final String QUERY_MAX = " MAX ";
    public static final String QUERY_COUNT = " COUNT ";
    public static final String QUERY_ALL = "*";
    public static final String QUERY_VALUES = " VALUES ";
    public static final String QUERY_UPDATE = "UPDATE ";
    public static final String QUERY_DELETE = "DELETE ";
    public static final String QUERY_SET = " SET ";
    public static final String QUERY_AND = " AND ";
    public static final String QUERY_IN = "IN";
    public static final String QUERY_OUT = "OUT";
    public static final String QUERY_INOUT = "INOUT";
    public static final String QUERY_CALL = "{CALL ";

    public static final String QUERY_TABLE_NUMBER_OF_ROWS = QUERY_SELECT + QUERY_COUNT + "(*)" + QUERY_FROM;

    public static final String TABLE_USER = "u_user";
    public static final String TABLE_ABILITY = "u_ability";
    public static final String TABLE_SCHEDULE = "u_schedule";
    public static final String TABLE_SPECIALITY_DOCTOR = "u_speciality_doctor";
    public static final String TABLE_DOCTOR = "u_doctor";
    public static final String TABLE_EMPLOYEE = "u_employee";
    public static final String TABLE_ASSITANT = "u_assistant";
    public static final String TABLE_SPECTIALITY = "speciality";
    public static final String TABLE_INVESTIGATION = "investigation";

    public static final String FXML = "/fxml";
    public static final String FXML_LOGIN =  FXML + "/login.fxml";
    public static final String FXML_ADMIN_MANAGEMENT = FXML + "/admin.fxml";
    public static final String FXML_TABLE_MANAGEMENT = FXML + "/tableManagement.fxml";
    public static final String FXML_HUMANRESOURCE =  FXML + "/humanresource.fxml";
    public static final String FXML_DIALOG = FXML + "/dialog.fxml";

    public static final String ICON_FILE_NAME = "/images/error.png";

    public static final String ROLE1 = "super-administrator";
    public static final String ROLE2 = "administrator";
    public static final String ROLE3 = "medic";
    public static final String ROLE4 = "asistent medical";
    public static final String ROLE5 = "receptioner";
    public static final String ROLE6 = "expert financiar-contabil";
    public static final String ROLE7 = "inspector resurse umane";

    public static final String [] USERS_ROLES = {ROLE1,  ROLE2, ROLE3, ROLE4 , ROLE5, ROLE6, ROLE7};

    public static final String [] DAY_OF_WEEK = {"","luni", "marti", "miercuri", "joi", "vineri", "simbata", "duminica"};

    public static final int COMBOBOX_WIDTH_MIN = 100;
    public static final int COMBOBOX_WIDTH_PREF = 150;
    public static final int COMBOBOX_WIDTH_MAX = 500;
    public static final int TEXTFIELD_WIDTH_PREF = 150;
    public static final int TEXTFIELD_HEIGHT_PREF = 25;
    public static final int LABEL_WIDTH_PREF = 130;
    public static final int LABEL_HEIGHT_PREF = 25;

    public static final String PATTERN_FORMAT_LOCALDATE_1 = "dd/MM/yyyy";
    public static final String PATTERN_FORMAT_LOCALDATE_2 = "yyyy-MM-dd";
}
