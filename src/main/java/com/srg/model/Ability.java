package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 7/10/15.
 */
public class Ability implements Serializable {

    private SimpleStringProperty id;
    private SimpleStringProperty name;
    private SimpleStringProperty id_speciality_doctor;

    public Ability(){
        this(null, null, null);
    }

    public Ability(String id, String name, String id_speciality_doctor) {
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.id_speciality_doctor = new SimpleStringProperty(id_speciality_doctor);
    }

    public Ability(ArrayList<String> values){
        this(values.get(0), values.get(1), values.get(2));
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getId_speciality_doctor() {
        return id_speciality_doctor.get();
    }

    public SimpleStringProperty id_speciality_doctorProperty() {
        return id_speciality_doctor;
    }

    public void setId_speciality_doctor(String id_speciality_doctor) {
        this.id_speciality_doctor.set(id_speciality_doctor);
    }

    public ArrayList<String> getValues(){
        ArrayList<String> values = new ArrayList<>();
        values.add(getId());
        values.add(getName());
        values.add(String.valueOf(getId_speciality_doctor()));
        return values;
    }

    public String toString(){
        return id.get() +
                " / " + name.get() +
                " / " + id_speciality_doctor.get();
    }

}
