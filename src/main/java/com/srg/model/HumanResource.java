package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by administrator on 8/5/15.
 */
public class HumanResource {
    private SimpleStringProperty fname;
    private SimpleStringProperty lname;
    private SimpleStringProperty position;
    private SimpleStringProperty timeSlot;
    private SimpleStringProperty weekday;
    private SimpleStringProperty scheduleDate;
    private SimpleStringProperty location;

    private User user;
    private Schedule schedule;

    public HumanResource(){
        this(null, null, null, null, null, null, null);
    }

    public HumanResource(String fname, String lname, String position, String timeSlot, String weekday, String scheduleDate, String location) {
        this.fname = new SimpleStringProperty(fname);
        this.lname = new SimpleStringProperty(lname);
        this.position = new SimpleStringProperty(position);
        this.timeSlot = new SimpleStringProperty(timeSlot);
        this.weekday = new SimpleStringProperty(weekday);
        this.scheduleDate = new SimpleStringProperty(scheduleDate);
        this.location = new SimpleStringProperty(location);
    }

    public HumanResource(User user, Schedule schedule){
        this(user.getFirst_name(), user.getLast_name(), user.getPosition(),
                schedule.getTime_slot(), schedule.getWeekday(), schedule.getSchedule_date(), schedule.getLocation());
        this.user = user;
        this.schedule = schedule;
    }

    public String getFname() {
        return fname.get();
    }

    public SimpleStringProperty fnameProperty() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname.set(fname);
    }

    public String getLname() {
        return lname.get();
    }

    public SimpleStringProperty lnameProperty() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname.set(lname);
    }

    public String getPosition() {
        return position.get();
    }

    public SimpleStringProperty positionProperty() {
        return position;
    }

    public void setPosition(String position) {
        this.position.set(position);
    }

    public String getTimeSlot() {
        return timeSlot.get();
    }

    public SimpleStringProperty timeSlotProperty() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot.set(timeSlot);
    }

    public String getWeekday() {
        return weekday.get();
    }

    public SimpleStringProperty weekdayProperty() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday.set(weekday);
    }

    public String getScheduleDate() {
        return scheduleDate.get();
    }

    public SimpleStringProperty scheduleDateProperty() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate.set(scheduleDate);
    }

    public String getLocation() {
        return location.get();
    }

    public SimpleStringProperty locationProperty() {
        return location;
    }

    public void setLocation(String location) {
        this.location.set(location);
    }

    public User user(){
        return user;
    }

    public Schedule schedule(){
        return schedule;
    }
}
