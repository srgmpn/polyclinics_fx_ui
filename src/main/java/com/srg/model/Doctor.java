package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 7/10/15.
 */
public class Doctor implements Serializable {

    private SimpleStringProperty personal_identifier;
    private SimpleStringProperty paraph_code;
    private SimpleStringProperty scientific_title;
    private SimpleStringProperty didactic_chair;

    public Doctor(){
        this(null, null, null, null);
    }

    public Doctor(String personal_identifier, String paraphCode, String scientificTitle, String didacticChair) {
        this.personal_identifier = new SimpleStringProperty(personal_identifier);
        this.paraph_code = new SimpleStringProperty(paraphCode);
        this.scientific_title = new SimpleStringProperty(scientificTitle);
        this.didactic_chair = new SimpleStringProperty(didacticChair);
    }

    public Doctor(ArrayList<String> values){
        this(values.get(0), values.get(1), values.get(2), values.get(3));
    }

    public String getPersonal_identifier() {
        return personal_identifier.get();
    }

    public SimpleStringProperty personal_identifierProperty() {
        return personal_identifier;
    }

    public void setPersonal_identifier(String personal_identifier) {
        this.personal_identifier.set(personal_identifier);
    }

    public String getParaph_code() {
        return paraph_code.get();
    }

    public SimpleStringProperty paraph_codeProperty() {
        return paraph_code;
    }

    public void setParaph_code(String paraph_code) {
        this.paraph_code.set(paraph_code);
    }

    public String getScientific_title() {
        return scientific_title.get();
    }

    public SimpleStringProperty scientific_titleProperty() {
        return scientific_title;
    }

    public void setScientific_title(String scientific_title) {
        this.scientific_title.set(scientific_title);
    }

    public String getDidactic_chair() {
        return didactic_chair.get();
    }

    public SimpleStringProperty didactic_chairProperty() {
        return didactic_chair;
    }

    public void setDidactic_chair(String didactic_chair) {
        this.didactic_chair.set(didactic_chair);
    }

    public ArrayList<String> getValues(){
        ArrayList<String> values = new ArrayList<>();
        values.add(getPersonal_identifier());
        values.add(getParaph_code());
        values.add(getScientific_title());
        values.add(getDidactic_chair());

        return values;
    }

    public String toString(){
        return personal_identifier.get() +
                " / " + paraph_code.get() +
                " / " + scientific_title.get() +
                " / " + didactic_chair.get();
    }

}
