package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;

/**
 * Created by administrator on 7/10/15.
 */
public class Investigation implements Serializable {

    private SimpleStringProperty id;
    private SimpleStringProperty name;
    private SimpleStringProperty duration;
    private SimpleStringProperty price;
    private SimpleStringProperty ability;
    private SimpleStringProperty id_speciality;

    public Investigation(){
        this(null, null, null, null, null, null);
    }

    public Investigation(String id, String name, String duration,
                         String price, String ability, String id_speciality){
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.duration = new SimpleStringProperty(duration);
        this.price = new SimpleStringProperty(price);
        this.ability = new SimpleStringProperty(ability);
        this.id_speciality = new SimpleStringProperty(id_speciality);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDuration() {
        return duration.get();
    }

    public SimpleStringProperty durationProperty() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration.set(duration);
    }

    public String getPrice() {
        return price.get();
    }

    public SimpleStringProperty priceProperty() {
        return price;
    }

    public void setPrice(String price) {
        this.price.set(price);
    }

    public String getAbility() {
        return ability.get();
    }

    public SimpleStringProperty abilityProperty() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability.set(ability);
    }

    public String getId_speciality() {
        return id_speciality.get();
    }

    public SimpleStringProperty id_specialityProperty() {
        return id_speciality;
    }

    public void setId_speciality(String id_speciality) {
        this.id_speciality.set(id_speciality);
    }

    public String toString(){
        return id.get() +
                " / " + name.get() +
                " / " + duration.get() +
                " / " + price.get() +
                " / " + ability.get() +
                " / " + id_speciality.get();
    }
}
