package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 7/10/15.
 */
public class User implements Serializable{

    private SimpleStringProperty personal_identifier;
    private SimpleStringProperty first_name;
    private SimpleStringProperty last_name;
    private SimpleStringProperty address;
    private SimpleStringProperty phone_number;
    private SimpleStringProperty email;
    private SimpleStringProperty IBAN_account;
    private SimpleStringProperty contract_number;
    private SimpleStringProperty hire_date;
    private SimpleStringProperty position;
    private SimpleStringProperty username;
    private SimpleStringProperty password;

    public User(){
        this(null, null, null, null, null, null, null, null, null, null, null, null);
    }


    public User(String personal_identifier, String first_name, String last_name,
                String address, String phone_number, String email, String IBAN_account,
                String contract_number, String hire_date, String position,
                String username, String password) {
        this.personal_identifier = new SimpleStringProperty(personal_identifier);
        this.first_name = new SimpleStringProperty(first_name);
        this.last_name = new SimpleStringProperty(last_name);
        this.address = new SimpleStringProperty(address);
        this.phone_number = new SimpleStringProperty(phone_number);
        this.email = new SimpleStringProperty(email);
        this.IBAN_account = new SimpleStringProperty(IBAN_account);
        this.contract_number = new SimpleStringProperty(contract_number);
        this.hire_date = new SimpleStringProperty(hire_date);
        this.position = new SimpleStringProperty(position);
        this.username = new SimpleStringProperty(username);
        this.password = new SimpleStringProperty(password);
    }

    public User(ArrayList<String> values){
        this(values.get(0), values.get(1), values.get(2),
                values.get(3), values.get(4), values.get(5),
                values.get(6), values.get(7), values.get(8),
                values.get(9), values.get(10), values.get(11));
    }

    public String getPersonal_identifier() {
        return personal_identifier.get();
    }

    public SimpleStringProperty personal_identifierProperty() {
        return personal_identifier;
    }

    public void setPersonal_identifier(String personal_identifier) {
        this.personal_identifier.set(personal_identifier);
    }

    public String getFirst_name() {
        return first_name.get();
    }

    public SimpleStringProperty first_nameProperty() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name.set(first_name);
    }

    public String getLast_name() {
        return last_name.get();
    }

    public SimpleStringProperty last_nameProperty() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name.set(last_name);
    }

    public String getAddress() {
        return address.get();
    }

    public SimpleStringProperty addressProperty() {
        return address;
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public String getPhone_number() {
        return phone_number.get();
    }

    public SimpleStringProperty phone_numberProperty() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number.set(phone_number);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getIBAN_account() {
        return IBAN_account.get();
    }

    public SimpleStringProperty IBAN_accountProperty() {
        return IBAN_account;
    }

    public void setIBAN_account(String IBAN_account) {
        this.IBAN_account.set(IBAN_account);
    }

    public String getContract_number() {
        return contract_number.get();
    }

    public SimpleStringProperty contract_numberProperty() {
        return contract_number;
    }

    public void setContract_number(String contract_number) {
        this.contract_number.set(contract_number);
    }

    public String getHire_date() {
        return hire_date.get();
    }

    public SimpleStringProperty hire_dateProperty() {
        return hire_date;
    }

    public void setHire_date(String hire_date) {
        this.hire_date.set(hire_date);
    }

    public String getPosition() {
        return position.get();
    }

    public SimpleStringProperty positionProperty() {
        return position;
    }

    public void setPosition(String position) {
        this.position.set(position);
    }

    public String getUsername() {
        return username.get();
    }

    public SimpleStringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getPassword() {
        return password.get();
    }

    public SimpleStringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public ArrayList<String> getValues(){
        ArrayList<String> values = new ArrayList<>(11);
        values.add(getPersonal_identifier());
        values.add(getFirst_name());
        values.add(getLast_name());
        values.add(getAddress());
        values.add(getPhone_number());
        values.add(getEmail());
        values.add(getIBAN_account());
        values.add(getContract_number());
        values.add(getHire_date());
        values.add(getPosition());
        values.add(getUsername());
        values.add(getPassword());
        return values;
    }

    public String toString(){
        return personal_identifier.get() +
                " / " + first_name.get() +
                " / " + last_name.get() +
                " / " + address.get() +
                " / " + phone_number.get() +
                " / " + email.get() +
                " / " + IBAN_account.get() +
                " / " + contract_number.get() +
                " / " + hire_date.get() +
                " / " + position.get() +
                " / " + username.get() +
                " / " + password.get();
    }
}
