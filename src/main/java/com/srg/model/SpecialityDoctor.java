package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 7/10/15.
 */
public class SpecialityDoctor implements Serializable {

    private SimpleStringProperty id;
    private SimpleStringProperty speciality;
    private SimpleStringProperty degree;
    private SimpleStringProperty id_doctor;

    public SpecialityDoctor(){
        this(null, null, null, null);
    }

    public SpecialityDoctor(String id, String speciality, String degree, String id_doctor){
        this.id = new SimpleStringProperty(id);
        this.speciality = new SimpleStringProperty(speciality);
        this.degree = new SimpleStringProperty(degree);
        this.id_doctor = new SimpleStringProperty(id_doctor);
    }

    public SpecialityDoctor(ArrayList<String> values){
        this(values.get(0), values.get(1), values.get(2), values.get(3));
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getSpeciality() {
        return speciality.get();
    }

    public SimpleStringProperty specialityProperty() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality.set(speciality);
    }

    public String getDegree() {
        return degree.get();
    }

    public SimpleStringProperty degreeProperty() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree.set(degree);
    }

    public String getId_doctor() {
        return id_doctor.get();
    }

    public SimpleStringProperty id_doctorProperty() {
        return id_doctor;
    }

    public void setId_doctor(String id_doctor) {
        this.id_doctor.set(id_doctor);
    }

    public ArrayList<String> getValues(){
        ArrayList<String> values = new ArrayList<>();
        values.add(getId());
        values.add(getSpeciality());
        values.add(getDegree());
        values.add(getId_doctor());
        return values;
    }

    public String toString(){
        return id.get() +
                " / " + speciality.get() +
                " / " + degree.get() +
                " / " + id_doctor.get();
    }
}
