package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;

/**
 * Created by administrator on 7/10/15.
 */
public class Speciality implements Serializable {

    private SimpleStringProperty id;
    private SimpleStringProperty name;

    public Speciality(){
        this(null, null);
    }

    public Speciality(String id, String name){
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String toString(){
        return id.get() +
                " / " + name.get();
    }
}
