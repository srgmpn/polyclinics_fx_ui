package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 7/10/15.
 */
public class Schedule implements Serializable {

    private SimpleStringProperty id;
    private SimpleStringProperty time_slot;
    private SimpleStringProperty weekday;
    private SimpleStringProperty schedule_date;
    private SimpleStringProperty location;
    private SimpleStringProperty id_employee;

    public Schedule(){
        this(null, null, null, null, null, null);
    }

    public Schedule(String id, String time_slot, String weekday,
                    String schedule_date, String location, String id_employee){
        this.id = new SimpleStringProperty(id);
        this.time_slot = new SimpleStringProperty(time_slot);
        this.weekday = new SimpleStringProperty(weekday);
        this.schedule_date = new SimpleStringProperty(schedule_date);
        this.location = new SimpleStringProperty(location);
        this.id_employee = new SimpleStringProperty(id_employee);
    }

    public Schedule(ArrayList<String> values){
        this(values.get(0), values.get(1), values.get(2),
                values.get(3), values.get(4), values.get(5));
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getTime_slot() {
        return time_slot.get();
    }

    public SimpleStringProperty time_slotProperty() {
        return time_slot;
    }

    public void setTime_slot(String time_slot) {
        this.time_slot.set(time_slot);
    }

    public String getWeekday() {
        return weekday.get();
    }

    public SimpleStringProperty weekdayProperty() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday.set(weekday);
    }

    public String getSchedule_date() {
        return schedule_date.get();
    }

    public SimpleStringProperty schedule_dateProperty() {
        return schedule_date;
    }

    public void setSchedule_date(String schedule_date) {
        this.schedule_date.set(schedule_date);
    }

    public String getLocation() {
        return location.get();
    }

    public SimpleStringProperty locationProperty() {
        return location;
    }

    public void setLocation(String location) {
        this.location.set(location);
    }

    public String getId_employee() {
        return id_employee.get();
    }

    public SimpleStringProperty id_employeeProperty() {
        return id_employee;
    }

    public void setId_employee(String id_employee) {
        this.id_employee.set(id_employee);
    }

    public ArrayList<String> getValues(){
        ArrayList<String> values = new ArrayList<>();
        values.add(getId());
        values.add(getTime_slot());
        values.add(getWeekday());
        values.add(getSchedule_date());
        values.add(getLocation());
        values.add(getId_employee());
        return values;
    }

    public String toString(){
        return id.get() +
                " / " + time_slot.get() +
                " / " + weekday.get() +
                " / " + schedule_date.get() +
                " / " + location.get() +
                " / " + id_employee.get();
    }
}
