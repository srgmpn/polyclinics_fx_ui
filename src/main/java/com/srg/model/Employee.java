package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 7/10/15.
 */
public class Employee implements Serializable {

    private SimpleStringProperty personal_identifier;
    private SimpleStringProperty salary;
    private SimpleStringProperty working_hours;

    public Employee(){
        this(null, null, null);
    }

    public Employee(String personal_identifier, String salary, String working_hours){
        this.personal_identifier = new SimpleStringProperty(personal_identifier);
        this.salary = new SimpleStringProperty(salary);
        this.working_hours = new SimpleStringProperty(working_hours);
    }

    public Employee(ArrayList<String> values){
        this(values.get(0), values.get(1), values.get(2));
    }

    public String getPersonal_identifier() {
        return personal_identifier.get();
    }

    public SimpleStringProperty personal_identifierProperty() {
        return personal_identifier;
    }

    public void setPersonal_identifier(String personal_identifier) {
        this.personal_identifier.set(personal_identifier);
    }

    public String getSalary() {
        return salary.get();
    }

    public SimpleStringProperty salaryProperty() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary.set(salary);
    }

    public String getWorking_hours() {
        return working_hours.get();
    }

    public SimpleStringProperty working_hoursProperty() {
        return working_hours;
    }

    public void setWorking_hours(String working_hours) {
        this.working_hours.set(working_hours);
    }

    public ArrayList<String> getValues(){
        ArrayList<String> values = new ArrayList<>(3);
        values.add(getPersonal_identifier());
        values.add(getSalary());
        values.add(getWorking_hours());
        return values;
    }

    public String toString(){
        return personal_identifier.get() +
                " / " + salary.get() +
                " / " + working_hours.get();
    }
}
