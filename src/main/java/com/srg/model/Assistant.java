package com.srg.model;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 7/10/15.
 */
public class Assistant implements Serializable {

    private SimpleStringProperty personal_identifier;
    private SimpleStringProperty speciality;
    private SimpleStringProperty degree;

    public Assistant(){
        this(null, null, null);
    }

    public Assistant(String personal_identifier, String speciality, String degree){
        this.personal_identifier = new SimpleStringProperty(personal_identifier);
        this.speciality = new SimpleStringProperty(speciality);
        this.degree = new SimpleStringProperty(degree);
    }

    public Assistant(ArrayList<String> values){
        this(values.get(0), values.get(1), values.get(2));
    }

    public String getPersonal_identifier() {
        return personal_identifier.get();
    }

    public SimpleStringProperty personal_identifierProperty() {
        return personal_identifier;
    }

    public void setPersonal_identifier(String personal_identifier) {
        this.personal_identifier.set(personal_identifier);
    }

    public String getSpeciality() {
        return speciality.get();
    }

    public SimpleStringProperty specialityProperty() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality.set(speciality);
    }

    public String getDegree() {
        return degree.get();
    }

    public SimpleStringProperty degreeProperty() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree.set(degree);
    }

    public ArrayList<String> getValues(){
        ArrayList<String> values = new ArrayList<>();
        values.add(getPersonal_identifier());
        values.add(getSpeciality());
        values.add(getDegree());
        return values;
    }

    public String toString(){
        return personal_identifier.get() +
                " / " + speciality.get() +
                " / " + degree.get();
    }
}
