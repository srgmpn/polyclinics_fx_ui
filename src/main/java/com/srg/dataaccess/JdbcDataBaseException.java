package com.srg.dataaccess;

import com.srg.util.Constants;

/**
 * Created by administrator on 7/11/15.
 */
public class JdbcDataBaseException extends Exception {

    private static final long serialVersionUID = 4096L;

    public JdbcDataBaseException(){
        super(Constants.MESSAGE);
    }

    public JdbcDataBaseException(String message){
        super(Constants.MESSAGE + " " + message);
    }

    public JdbcDataBaseException(Throwable cause){
        super(Constants.MESSAGE, cause);
    }

    public JdbcDataBaseException(String message, Throwable cause){
        super(Constants.MESSAGE + " " + message, cause);
    }
}
