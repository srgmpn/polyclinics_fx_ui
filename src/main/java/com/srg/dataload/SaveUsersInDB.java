package com.srg.dataload;

import com.srg.dao.BasicCRUDDao;
import com.srg.dao.impl.*;
import com.srg.model.*;
import com.srg.util.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by administrator on 7/16/15.
 */
public class SaveUsersInDB {

    private static BasicCRUDDao<User> userDao = new UserDaoImpl(Constants.TABLE_USER);
    private static BasicCRUDDao<Employee> employeeDao = new EmployeeDaoImpl(Constants.TABLE_EMPLOYEE);
    private static BasicCRUDDao<Schedule> scheduleDao = new ScheduleDaoImpl(Constants.TABLE_SCHEDULE);
    private static BasicCRUDDao<Assistant> assitantDao = new AssistantDaoImpl(Constants.TABLE_ASSITANT);
    private static BasicCRUDDao<Doctor> doctorDao = new DoctorDaoImpl(Constants.TABLE_DOCTOR);
    private static BasicCRUDDao<SpecialityDoctor> specialityDoctorDao = new SpecialityDoctorDaoImpl(Constants.TABLE_SPECIALITY_DOCTOR);
    private static BasicCRUDDao<Ability> abilityDao = new AbilityDaoImpl(Constants.TABLE_ABILITY);

    private SaveUsersInDB(){}

    public static void save(User user, Employee employee, ArrayList<Schedule> scheduleList, Assistant assistant, Doctor doctor,
                            ArrayList<SpecialityDoctor> specialityDoctorList, Map<String, ArrayList<Ability>> listMapAbilities){

        if (user == null){
            return;
        }

        try {
            userDao.add(user);

            if (employee != null){
                employeeDao.add(employee);
            }

            if (scheduleList != null && !scheduleList.isEmpty()){
                for (Schedule schedule : scheduleList){
                    scheduleDao.add(schedule);
                }
            }

            if (assistant != null){
                assitantDao.add(assistant);
            }

            if (doctor != null){
                doctorDao.add(doctor);
            }

            if (specialityDoctorList != null && !specialityDoctorList.isEmpty()){
                for (SpecialityDoctor specialityDoctor : specialityDoctorList){
                    specialityDoctorDao.add(specialityDoctor);
                }
            }

            if (listMapAbilities != null && !listMapAbilities.isEmpty()){

                for (ArrayList<Ability> abilities : listMapAbilities.values()){
                    if (abilities != null && !abilities.isEmpty()){

                        for (Ability ability : abilities) {
                            abilityDao.add(ability);
                        }
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
