package com.srg.dataload;

import com.srg.util.Constants;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by administrator on 7/10/15.
 */
public class LoadDatesInDB {

    private CountDownLatch latch;

    public LoadDatesInDB(CountDownLatch latch){
        this.latch = latch;
    }

    /**
     *start load users and medical service in DB
     * in 2 threads
     * @param filePathUsers
     * @param filePathMedicalService
     */
    public void load(String filePathUsers, String filePathMedicalService) {

        AbstractXPathParser parserMedicalService = new XPathHandlerMedicalService(filePathMedicalService);
        SaxUserParser parseUsers = new SaxUserParser(filePathUsers);
        ExecutorService service = Executors.newFixedThreadPool(2);

        if (PingDB.pingUsers()) {
            latch.countDown();

        } else {
            //load users dates
            service.submit(() -> {
                try {
                    if (Constants.DEBUG) {
                        System.out.println("-> Load Users... ");
                    }
                    parseUsers.parse();

                    if (Constants.DEBUG) {
                        System.out.println("-> Users are loaded");
                    }
                    latch.countDown();
                } catch (IOException | XMLStreamException | ParserConfigurationException | SAXException ex) {
                    ex.printStackTrace();
                }
            });
        }

        if (PingDB.pingMedicalService()) {
            latch.countDown();

        } else {
            //load investigations dates
            service.submit(() -> {
                try {
                    if (Constants.DEBUG) {
                        System.out.println("-> Load Medical Servicies ... ");
                    }
                    parserMedicalService.init();
                    parserMedicalService.parse();

                    if (Constants.DEBUG) {
                        System.out.println("-> Medical Servicies are loaded");
                    }
                    latch.countDown();
                } catch (IOException | ParserConfigurationException | SAXException | XPathExpressionException e) {
                    e.printStackTrace();
                }
            });

            service.shutdown();
            try {
                service.awaitTermination(5, TimeUnit.MINUTES);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
