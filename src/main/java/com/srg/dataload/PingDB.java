package com.srg.dataload;

import com.srg.dao.BasicCRUDDao;
import com.srg.dao.impl.*;
import com.srg.model.*;
import com.srg.util.Constants;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by administrator on 7/24/15.
 */
public class PingDB {

    private PingDB(){}

    public static boolean pingUsers(){
        BasicCRUDDao<User> userDao = new UserDaoImpl(Constants.TABLE_USER);

        try {
            List<User> userList = userDao.getAll();
            if (userList == null || userList.isEmpty()){
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static boolean pingMedicalService(){

        BasicCRUDDao<Speciality> daoSpeciality = new SpecialityDaoImpl(Constants.TABLE_SPECTIALITY);
        try {
            List<Speciality> specialitiesList = daoSpeciality.getAll();
            if (specialitiesList == null || specialitiesList.isEmpty()){
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
}
