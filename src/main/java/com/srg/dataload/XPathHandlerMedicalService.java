package com.srg.dataload;

import com.srg.dao.BasicCRUDDao;
import com.srg.dao.impl.InvestigationDaoImpl;
import com.srg.dao.impl.SpecialityDaoImpl;
import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.Investigation;
import com.srg.model.Speciality;
import com.srg.util.Constants;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by administrator on 7/22/15.
 */
public class XPathHandlerMedicalService extends AbstractXPathParser{

    private BasicCRUDDao<Speciality> daoSpeciality = new SpecialityDaoImpl(Constants.TABLE_SPECTIALITY);
    private BasicCRUDDao<Investigation> daoInvestigation = new InvestigationDaoImpl(Constants.TABLE_INVESTIGATION);
    Speciality specialityModel;

    public XPathHandlerMedicalService(String xmlFilePath){
        super(xmlFilePath);
    }

    @Override
    public void parse() throws XPathExpressionException {
        String xPathExpression = "/medical_services/medical_service";
        NodeList nodeList = (NodeList) xPath.compile(xPathExpression).evaluate(xmlDocument, XPathConstants.NODESET);

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nNode = nodeList.item(i);
           // System.out.println(nNode.getNodeName());
            String speciality = "";

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                if (Constants.DEBUG) {
                    System.out.println("Start load speciality ... ");
                }
                Element eElement = (Element) nNode;
                speciality = eElement
                        .getElementsByTagName("speciality")
                        .item(0)
                        .getTextContent();

               // System.out.println("\tSpeciality : " + speciality);
                specialityModel = new Speciality();
                specialityModel.setName(speciality);

                try {
                    daoSpeciality.add(specialityModel);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            String xPathInExpression = xPathExpression + "[speciality='" + speciality + "']/investigations/investigation";
            handleInNodeList(xPathInExpression);

            if (Constants.DEBUG) {
                System.out.println("Investigations of " + specialityModel.getName() + " loaded");
                System.out.println("Speciality : " + specialityModel.getName() + " loaded");
            }

            specialityModel = null;


        }
    }

    protected void handleInNodeList(String xPathExpression) throws XPathExpressionException {

        if (Constants.DEBUG) {
            System.out.println("Start load Investigations of : " + specialityModel.getName() + " ... ");
        }

        NodeList nodeInvetsigation = (NodeList) xPath.compile(xPathExpression).evaluate(xmlDocument, XPathConstants.NODESET);

        for (int j = 0; j < nodeInvetsigation.getLength(); ++j) {
            Node iNode = nodeInvetsigation.item(j);

            if (iNode.getNodeType() == Node.ELEMENT_NODE) {
                Investigation investigation = new Investigation();
                //System.out.println("\t\t---------------------------------");
                Element iElement = (Element) iNode;
                String name = iElement
                                    .getElementsByTagName("name")
                                    .item(0)
                                    .getTextContent();
                investigation.setName(name);
               // System.out.println("\t\tName : " + name);
                String duration = iElement
                                    .getElementsByTagName("duration")
                                    .item(0)
                                    .getTextContent();
                investigation.setDuration(duration);
               // System.out.println("\t\tDuration : " + duration);
                String price = iElement
                                    .getElementsByTagName("price")
                                    .item(0)
                                    .getTextContent();
                investigation.setPrice(price);
               // System.out.println("\t\tPrice : " + price);
                String ability = iElement
                                    .getElementsByTagName("ability")
                                    .item(0)
                                    .getTextContent();
                investigation.setAbility(ability);
               // System.out.println("\t\tAbility : " + ability);

                investigation.setId_speciality(getSpecialityId());
                //System.out.println(specialityModel);
               // System.out.println(investigation);

                try {
                    daoInvestigation.add(investigation);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                investigation = null;
            }
        }
    }

    /**
     * get Id of Speciality which last was generated in DB
     * @return - id
     */
    private String getSpecialityId(){


        if (specialityModel.getId() == null) {
            try {
                ArrayList<String> attributies = new ArrayList<>();
                attributies.add("MAX(id)");
                ArrayList<ArrayList<String>> content = JdbcDataBaseWrapperImpl.getInstance().getTableContent(Constants.TABLE_SPECTIALITY, attributies, null, null, null);
                if (content.get(0).get(0) != null && (!content.get(0).get(0).isEmpty())) {
                    specialityModel.setId(content.get(0).get(0));
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return specialityModel.getId();
    }
}
