package com.srg.dataload;

import com.srg.dataaccess.JdbcDataBaseWrapperImpl;
import com.srg.model.*;
import com.srg.util.Constants;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by administrator on 7/10/15.
 */
public class SaxHandlerUser extends DefaultHandler {

    //user fields
    private boolean bPersonalIdentifier = false;
    private boolean bFname = false;
    private boolean bLname = false;
    private boolean bTown = false;
    private boolean bCountry = false;
    private boolean bPhoneNumber = false;
    private boolean bEmail = false;
    private boolean bIBAN = false;
    private boolean bContractNumber = false;
    private boolean bHireDate = false;
    private boolean bPosition = false;
    private boolean bUserName = false;
    private boolean bPassword = false;
    //employee
    private boolean bSalary = false;
    private boolean bWorkingHourse = false;
    //schedule
    private boolean bSchedule = false;
    private boolean bWeekDay = false;
    private boolean bTimeSlot = false;
    private boolean bLocation = false;
    private boolean bScheduleDate = false;
    //doctor
    private boolean bParaphCode = false;
    private boolean bScientificTitle = false;
    private boolean bDidacticChair = false;
    //SpecialityDoctor
    private boolean bDSpecialities = false;
    private boolean bDSpecialitie = false;
    private boolean bDSpecialityName = false;
    private boolean bDSpecialityDegree = false;
    //ability
    private boolean bAbilities = false;
    private boolean bAbilityName = false;
    //assistant
    private boolean bAssistentSpeciality = false;
    private boolean bAssistentDegree = false;

    private boolean b = false;

    private User user;
    private Doctor doctor;
    private Assistant assistant;
    private Ability ability;
    private Schedule schedule;
    private Employee employee;
    private SpecialityDoctor specialityDoctor;

    private ArrayList<Ability> abilitiesList;
    private ArrayList<SpecialityDoctor> specialityDoctorList;
    private Map<String, ArrayList<Ability>> listMapAbilities;
    private ArrayList<Schedule> scheduleList;

    @Override
    public void startElement(String uri, String localName,
                             String qName, Attributes attributes) throws SAXException {
        //System.out.println("Start Element :" + qName);
        //--------------   user -----------------
        if (qName.equalsIgnoreCase("user")){
            //create new user
            user = new User();
            b = true;

            if (Constants.DEBUG){
                System.out.println("Start LOAD USER ... ");
            }
        }

        if (qName.equalsIgnoreCase("personal_identifier")){
            bPersonalIdentifier = true;
        }

        if (qName.equalsIgnoreCase("first_name")){
            bFname = true;
        }

        if (qName.equalsIgnoreCase("last_name")){
            bLname = true;
        }

        if (qName.equalsIgnoreCase("town")){
            bTown = true;
        }

        if (qName.equalsIgnoreCase("county")){
            bCountry = true;
        }

        if (qName.equalsIgnoreCase("phone_number")){
            bPhoneNumber = true;
        }

        if (qName.equalsIgnoreCase("email")){
            bEmail = true;
        }

        if (qName.equalsIgnoreCase("IBAN_account")){
            bIBAN = true;
        }

        if (qName.equalsIgnoreCase("contract_number")){
            bContractNumber = true;
        }
        if (qName.equalsIgnoreCase("hire_date")){
            bHireDate = true;
        }
        if (qName.equalsIgnoreCase("position")){
            bPosition = true;
        }

        if (qName.equalsIgnoreCase("username")){
            bUserName = true;
        }

        if (qName.equalsIgnoreCase("password")){
            bPassword = true;
        }

        //--------- employee ------------------------
        if (qName.equalsIgnoreCase("salary")){
            bSalary = true;
            employee = new Employee();
        }

        if (qName.equalsIgnoreCase("working_hours")){
            bWorkingHourse = true;
        }

        //--------- schedule ------------------------
        if (qName.equalsIgnoreCase("timetable")){
            scheduleList = new ArrayList<>();
        }

        if (qName.equalsIgnoreCase("schedule")){
            //create a new Schedule
            bSchedule = true;
            schedule = new Schedule();
        }

        if (bSchedule && qName.equalsIgnoreCase("time_slot")){
            bTimeSlot = true;
        }

        if (bSchedule && qName.equalsIgnoreCase("weekday")){
            bWeekDay = true;
        }

        if (bSchedule && qName.equalsIgnoreCase("location")){
            bLocation = true;
        }

        if (bSchedule && qName.equalsIgnoreCase("date")){
            bScheduleDate = true;
        }


        //----------- assistant -----------------------
        if (!bDSpecialities && qName.equalsIgnoreCase("speciality")){
            assistant = new Assistant();
            bAssistentSpeciality = true;
        }

        if (!bDSpecialities && qName.equalsIgnoreCase("degree")){
            bAssistentDegree = true;
        }

        //---------- doctor -------------------------
        if (qName.equalsIgnoreCase("paraph_code")){
            bParaphCode = true;
            doctor = new Doctor();
        }

        if (qName.equalsIgnoreCase("scientific_title")){
            bScientificTitle = true;
        }

        if (qName.equalsIgnoreCase("didactic_chair")){
            bDidacticChair = true;
        }

        //--------- specialityDoctor ------------------
        if (qName.equalsIgnoreCase("specialities")){
            bDSpecialities = true;
            specialityDoctorList = new ArrayList<>();
            listMapAbilities = new LinkedHashMap<>();
        }

        if (bDSpecialities && qName.equalsIgnoreCase("speciality")){
            bDSpecialitie = true;
            b = true;
            specialityDoctor = new SpecialityDoctor();
        }

        if (b && bDSpecialities && bDSpecialitie && qName.equalsIgnoreCase("name")){
            bDSpecialityName = true;
        }

        if (bDSpecialities && bDSpecialitie && qName.equalsIgnoreCase("degree")){
            bDSpecialityDegree = true;
        }

        //----------- abilities ------------------------
        if (bDSpecialities && bDSpecialitie && qName.equalsIgnoreCase("abilities")){
            bAbilities = true;
            abilitiesList = new ArrayList<>();
        }

        if (bAbilities && qName.equalsIgnoreCase("name")){
            ability = new Ability();
            bAbilityName = true;
        }
    }

    @Override
    public void endElement(String uri, String localName,
                           String qName) throws SAXException {

        if (qName.equalsIgnoreCase("user")){
            SaveUsersInDB.save(user, employee, scheduleList, assistant, doctor, specialityDoctorList, listMapAbilities);

            if (Constants.DEBUG){
                System.out.println("--> USER CNP: " + user.getPersonal_identifier() + " ---> position : " + user.getPosition());
                System.out.println("--------- USER LOADED -------------------");
            }

            scheduleList = null;
            specialityDoctorList = null;
            listMapAbilities = null;
            doctor = null;
            assistant = null;
            employee = null;
            user = null;
        }

        if (qName.equalsIgnoreCase("schedule")){
            bSchedule = false;
            schedule.setId_employee(employee.getPersonal_identifier());
            scheduleList.add(schedule);
            schedule = null;
        }

        if (qName.equalsIgnoreCase("specialities")){
            bDSpecialities = false;
        }

        if (bDSpecialities && qName.equalsIgnoreCase("speciality")){
            specialityDoctorList.add(specialityDoctor);
            bDSpecialitie = false;
            b = false;
            specialityDoctor = null;
        }

        if (qName.equalsIgnoreCase("abilities")){

            listMapAbilities.put(specialityDoctor.getId(), abilitiesList);
            abilitiesList = null;
            bAbilities = false;
        }

        if (bAbilities && qName.equalsIgnoreCase("name")){
            ability.setId_speciality_doctor(specialityDoctor.getId());
            abilitiesList.add(ability);
            ability = null;
        }
    }

    @Override
    public void characters(char ch[], int start, int length)
            throws SAXException {
        //---------- user -----------------------------------
        if (bPersonalIdentifier){
            user.setPersonal_identifier(new String(ch, start, length));
            bPersonalIdentifier = false;
        }

        if (bFname){
            user.setFirst_name(new String(ch, start, length));
            bFname = false;
        }

        if (bLname){
            user.setLast_name(new String(ch, start, length));
            bLname = false;
        }

        if (bTown){
            user.setAddress(new String(ch, start, length));
            bTown = false;
        }

        if (bCountry){
            String address = user.getAddress() + ", " + new String(ch, start, length);
            user.setAddress(address);
            bCountry = false;
        }

        if (bPhoneNumber){
            user.setPhone_number(new String(ch, start, length));
            bPhoneNumber = false;
        }

        if (bEmail){
            user.setEmail(new String(ch, start, length));
            bEmail = false;
        }

        if (bIBAN){
            user.setIBAN_account(new String(ch, start, length));
            bIBAN = false;
        }

        if (bContractNumber){
            user.setContract_number(new String(ch, start, length));
            bContractNumber = false;
        }

        if (bHireDate){
            user.setHire_date(new String(ch, start, length));
            bHireDate = false;
        }

        if (bPosition){
            bPosition = false;
            user.setPosition(new String(ch, start, length));
        }

        if (bUserName){
            bUserName = false;
            user.setUsername(new String(ch, start, length));
        }

        if (bPassword){
            bPassword = false;
            user.setPassword(new String(ch, start, length));
        }

        //--------------- employee ------------------------------------
        if (bSalary){
            employee.setPersonal_identifier(user.getPersonal_identifier());
            employee.setSalary(new String(ch, start, length));
            bSalary = false;
        }

        if (bWorkingHourse){
            employee.setWorking_hours(new String(ch, start, length));
            bWorkingHourse = false;
        }

        //-------------- schedule -------------------------------------
        if (bTimeSlot){
            schedule.setTime_slot(new String(ch, start, length));
            bTimeSlot = false;
        }

        if (bWeekDay){
            schedule.setWeekday(new String(ch, start, length));
            bWeekDay = false;
        }

        if (bLocation){
            schedule.setLocation(new String(ch, start, length));
            bLocation = false;
        }

        if (bScheduleDate){
            schedule.setSchedule_date(new String(ch, start, length));
            bScheduleDate = false;
        }

        //------------------- assistant -------------------------------
        if (bAssistentSpeciality){
            assistant.setPersonal_identifier(employee.getPersonal_identifier());
            assistant.setSpeciality(new String(ch, start, length));
            bAssistentSpeciality = false;
        }

        if (bAssistentDegree){
            assistant.setDegree(new String(ch, start, length));
            bAssistentDegree = false;
        }

        //-------------------- doctor ---------------------------------
        if (bParaphCode){
            doctor.setPersonal_identifier(employee.getPersonal_identifier());
            doctor.setParaph_code(new String(ch, start, length));
            bParaphCode = false;
        }

        if (bScientificTitle){
            doctor.setScientific_title(new String(ch, start, length));
            bScientificTitle = false;
        }

        if (bDidacticChair){
            doctor.setDidactic_chair(new String(ch, start, length));
            bDidacticChair = false;
        }

        //-------------------- specialityDoctor -----------------------
        if (bDSpecialityName){
            //get the SpecialityDoctor ID is DB
            if (specialityDoctor.getId() == null) {
                specialityDoctor.setId(getSpecialityId());
            }

            specialityDoctor.setSpeciality(new String(ch, start, length));
            bDSpecialityName = false;
            b = false;
        }

        if (bDSpecialityDegree){
            specialityDoctor.setDegree(new String(ch, start, length));
            specialityDoctor.setId_doctor(doctor.getPersonal_identifier());
            bDSpecialityDegree = false;
        }

        //------------------- ability --------------------------------
        if (bAbilityName){
            ability.setName(new String(ch, start, length));
            bAbilityName = false;
        }
    }

    private String getSpecialityId(){
        String nrRows = "1";
        try {

            ArrayList<String> attributies = new ArrayList<>();
            attributies.add("MAX(id)");
            ArrayList<ArrayList<String>> content = JdbcDataBaseWrapperImpl.getInstance().getTableContent(Constants.TABLE_SPECIALITY_DOCTOR, attributies, null, null, null);
            if (content.get(0).get(0) != null && (!content.get(0).get(0).isEmpty())) {
                nrRows = content.get(0).get(0);
                int nrR = Integer.parseInt(nrRows);
                nrRows = "" + (++nrR);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nrRows;
    }
}
