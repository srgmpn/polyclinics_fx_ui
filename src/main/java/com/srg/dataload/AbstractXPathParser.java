package com.srg.dataload;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by administrator on 7/24/15.
 */
public abstract class AbstractXPathParser {

    protected Document xmlDocument;
    protected XPath xPath;
    private String xmlFilePath;

    public AbstractXPathParser(String xmlFilePath) {
        this.xmlFilePath = xmlFilePath;
    }

    public void init() throws IOException, ParserConfigurationException, SAXException {
        if (Files.exists(Paths.get(xmlFilePath))) {

            FileInputStream file = new FileInputStream(new File(xmlFilePath));
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();

            xmlDocument = builder.parse(file);
            xPath = XPathFactory.newInstance().newXPath();

        } else {
            throw new FileNotFoundException("File " + xmlFilePath);
        }
    }

    public abstract void parse() throws XPathExpressionException;
    protected abstract void handleInNodeList(String xPathExpression) throws XPathExpressionException;
}
