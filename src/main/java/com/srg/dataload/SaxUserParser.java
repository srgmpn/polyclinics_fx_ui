package com.srg.dataload;

import com.srg.util.Constants;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by administrator on 7/24/15.
 */
public class SaxUserParser {

    private String filePath;

    public SaxUserParser(String filePath){
        this.filePath = filePath;
    }

    /**
     *use sAX parser for parse xml
     */
    public void parse() throws IOException, XMLStreamException, ParserConfigurationException, SAXException {

        if (Files.exists(Paths.get(filePath))) {

            if (Constants.DEBUG){
                System.out.println("Start parse users.xml ... ");
            }

            SAXParserFactory factory = SAXParserFactory.newInstance();

            InputStream xmlInput = new FileInputStream(filePath);

            SAXParser saxParser = factory.newSAXParser();
            SaxHandlerUser handler = new SaxHandlerUser();
            saxParser.parse(xmlInput, handler);

        } else {
            throw new FileNotFoundException("file " + filePath);
        }
    }
}
