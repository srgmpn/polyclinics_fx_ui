package com.srg.core;

import com.srg.controller.LoginController;
import com.srg.dataload.LoadDatesInDB;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.concurrent.CountDownLatch;

/**
 * Created by administrator on 7/10/15.
 */
public class App  extends Application{

    public static void main(String[] args) {

        CountDownLatch latch = new CountDownLatch(2);
        //load dates in DB
        new LoadDatesInDB(latch).load("scripts/users.xml", "scripts/medical_services.xml");
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        launch(args);

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        new LoginController().start();
    }
}