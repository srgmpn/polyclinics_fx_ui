package com.srg.core;

import com.srg.controller.HumanResourceController;
import com.srg.controller.AdminController;
import com.srg.model.User;
import com.srg.util.Constants;

/**
 * Created by administrator on 7/26/15.
 */
public class RBACHandle {

    private static RBACHandle INSTANCE;
    private User user;

    private RBACHandle(){
    }

    public static RBACHandle getInstance(){
        if (INSTANCE == null){
            INSTANCE = new RBACHandle();
        }
        return INSTANCE;
    }

    public void handle(String userRole){

        switch (userRole){
            case Constants.ROLE1:
                System.out.println("--> super-administrator");
                new AdminController().start();
                break;
            case Constants.ROLE2:
                System.out.println("--> administrator");
                new AdminController().start();
                break;
            case Constants.ROLE3:
                System.out.println("--> medic");
                break;
            case Constants.ROLE4:
                System.out.println("--> asistent medical");
                break;
            case Constants.ROLE5:
                System.out.println("--> receptioner");
                break;
            case Constants.ROLE6:
                System.out.println("--> expert financiar-contabil");
                break;
            case Constants.ROLE7:
                System.out.println("--> inspector resurse umane");
                new HumanResourceController().start();
                break;
        }
    }

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }
}
